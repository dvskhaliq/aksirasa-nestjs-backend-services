import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe, VersioningType } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { LoggerService } from './common/logs-exception/log-exception';
import { WinstonModule } from 'nest-winston';
import { PrismaClientExceptionFilter } from './common/prisma-client-exception/prisma-client-exception.filter';
import { ConfigService } from '@nestjs/config';
import { SocketIoAdapter } from './SocketIoAdapter';
// import * as bodyParser from 'body-parser';

async function bootstrap() {
  //set global custome logger
  const customeLoggerService = new LoggerService();

  const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger(customeLoggerService.createLoggerConfig),
  });

  // set global prefix
  app.setGlobalPrefix('api');
  // set versioning
  app.enableVersioning({
    type: VersioningType.URI,
  });
  //  init Global Pipe
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );

  //set size json request
  // app.use(bodyParser.json({ limit: '50mb' }));
  // app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

  //Set Config For Socket.io
  const configService = app.get(ConfigService);
  const port = parseInt(configService.get('PORT'));
  const clientPort = parseInt(configService.get('CLIENT_PORT'));
  const clientIP = configService.get('CLIENT_DOMAIN');

  app.enableCors({
    origin: [
      `http://${clientIP}:${clientPort}`,
      '192.168.56.1',
      '192.168.113.197',
      new RegExp(`/^http:\/\/192\.168\.1\.([1-9]|[1-9]\d):${clientPort}$/`),
      `http://localhost:${clientPort}`,
    ],
  });
  app.useWebSocketAdapter(new SocketIoAdapter(app, configService));

  // set Backend info
  const config = new DocumentBuilder()
    .setTitle('AksirasaRestAPI')
    .setDescription('Aksirasa Backend Services')
    .setVersion('1.0')
    .build();

  // set swagger init
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api/swagger', app, document);

  // set global exception handler
  //prisma exception
  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new PrismaClientExceptionFilter(httpAdapter));

  app.useWebSocketAdapter(new SocketIoAdapter(app, configService));
  await app.listen(port);
}
bootstrap();
