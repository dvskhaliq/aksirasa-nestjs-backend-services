import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  TransactionOpenDto,
  TransactionClosedDto,
} from '../../services/transaction/dto/index';
import axios from 'axios';
import * as crypto from 'crypto';

@Injectable()
export class TripayService {
  private logger = new Logger(TripayService.name);
  constructor(private readonly config: ConfigService) {}

  async getIntruksiPembayaran(code_va: string) {
    const apiUrl = this.config.get<string>('GATEWAY_PAYMENT');
    const apiKey = this.config.get<string>('API_KEY');
    const url = `${apiUrl}payment/instruction`;
    try {
      const getPaymentIntruction = await axios.get(url, {
        headers: { Authorization: apiKey },
        params: {
          code: code_va,
        },
      });
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull Get Data',
          status_code: 200,
        },
        data: getPaymentIntruction.data.data,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: `Exeption Handler Looging for ${err.message}`,
        err: err,
        errCustomCode: '20',
      });
      throw new Error(`Failed to fetch data from ${url}: ${err.message}`);
    }
  }
  async getPaymentChannel() {
    const apiUrl = this.config.get<string>('GATEWAY_PAYMENT');
    const apiKey = this.config.get<string>('API_KEY');
    const url = `${apiUrl}merchant/payment-channel`;

    try {
      const getPaymentChannel = await axios.get(url, {
        headers: { Authorization: apiKey },
      });
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull Get Data',
          status_code: 200,
        },
        data: getPaymentChannel.data.data,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: `Exeption Handler Looging for ${err.message}`,
        err: err,
        errCustomCode: '20',
      });
      throw new Error(`Failed to fetch data from ${url}: ${err.message}`);
    }
  }

  async getDetailTrasaksi(reference_code: string) {
    const apiUrl = this.config.get<string>('GATEWAY_PAYMENT');
    const apiKey = this.config.get<string>('API_KEY');
    const url = `${apiUrl}transaction/detail`;

    try {
      const getDetailTrasaksi = await axios.get(url, {
        headers: { Authorization: apiKey },
        data: { reference: reference_code },
      });
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull Get Data',
          status_code: 200,
        },
        data: getDetailTrasaksi.data.data,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: `Exeption Handler Looging for ${err.message}`,
        err: err,
        errCustomCode: '20',
      });
      throw new Error(`Failed to fetch data from ${url}: ${err.message}`);
    }
  }

  async requestTransaksiClosed(transactionClosedDto: TransactionClosedDto) {
    const apiUrl = this.config.get<string>('GATEWAY_PAYMENT');
    const apiKey = this.config.get<string>('API_KEY');
    const url = `${apiUrl}transaction/create`;
    const private_key = this.config.get<string>('PRIVATE_KEY');
    const merchant_code = this.config.get<string>('MERCHANT_CODE');
    const transactionCode = transactionClosedDto.merchan_ref;

    try {
      const expiredTime = await this.getExpiredTimeEpoch();
      const total_pembayaran: number = +transactionClosedDto.total_pembayaran;
      const getSignature = await this.CreateSignatureClosed(
        private_key,
        merchant_code,
        transactionCode,
        total_pembayaran,
      );
      const requestclosedtransaksi = await axios.post(
        url,
        {
          method: transactionClosedDto.metode_pembayaran,
          merchant_ref: transactionCode,
          amount: total_pembayaran,
          customer_name: transactionClosedDto.customer_name,
          customer_email: transactionClosedDto.customer_email,
          customer_phone: transactionClosedDto.customer_phone,
          order_items: transactionClosedDto.order_items,
          callback_url: transactionClosedDto.callback_url,
          expired_time: expiredTime,
          signature: getSignature,
        },
        {
          headers: {
            Authorization: apiKey,
            'Content-Type': 'application/json',
          },
        },
      );
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull Get Data',
          status_code: 200,
        },
        data: requestclosedtransaksi.data.data,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: `Exeption Handler Looging for ${err.message}`,
        err: err,
        errCustomCode: '20',
      });
      throw new Error(`Failed to fetch data from ${url}: ${err.message}`);
    }
  }
  async requestTransaksiOpen(RequestTransactionOpenDto: TransactionOpenDto) {
    const apiUrl = this.config.get<string>('GATEWAY_PAYMENT');
    const apiKey = this.config.get<string>('API_KEY');
    const url = `${apiUrl}open-payment/create`;
    const private_key = this.config.get<string>('PRIVATE_KEY');
    const merchant_code = this.config.get<string>('MERCHANT_CODE');
    try {
      const getSignature = await this.CreateSignatureOpen(
        private_key,
        merchant_code,
        RequestTransactionOpenDto.kode_transaksi,
        RequestTransactionOpenDto.metode_pembayaran,
      );
      const requestopentransaksi = await axios.post(
        url,
        {
          method: RequestTransactionOpenDto.metode_pembayaran,
          merchant_ref: RequestTransactionOpenDto.kode_transaksi,
          customer_name: RequestTransactionOpenDto.customer_name,
          signature: getSignature,
        },
        {
          headers: {
            Authorization: apiKey,
            'Content-Type': 'application/json',
          },
        },
      );
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull Get Data',
          status_code: 200,
        },
        data: requestopentransaksi.data.data,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: `Exeption Handler Looging for ${err.message}`,
        err: err,
        errCustomCode: '20',
      });
      throw new Error(`Failed to fetch data from ${url}: ${err.message}`);
    }
  }
  private async CreateSignatureOpen(
    privateKey: string,
    merchantCode: string,
    channel: string,
    merchantRef: string,
  ): Promise<string> {
    try {
      // const private_key = 'private_key_anda';
      // const merchant_code = 'T0001';
      // const channel = 'BCAVA';
      // const merchantRef = 'INV55567';
      const private_key = privateKey;
      const merchant_code = merchantCode;
      const channel_payment = channel;
      const merchant_ref = merchantRef;

      const data = `${merchant_code}${channel_payment}${merchant_ref}`;
      const signature = crypto
        .createHmac('sha256', private_key)
        .update(data)
        .digest('hex');
      return signature;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: `Exeption Handler Looging for ${err.message}`,
        err: err,
        errCustomCode: '20',
      });
      throw new Error(`Failed to create Signature : ${err.message}`);
    }
  }

  private async CreateSignatureClosed(
    private_key: string,
    merchant_code: string,
    merchant_ref: string,
    amountOf: number,
  ): Promise<string> {
    try {
      // const privateKey = 'ytf6ooi2gmlNPfpchd94jDOk8hRWOu';
      // const merchantCode = 'T0001';
      // const merchantRef = 'INV55567';
      // const amount = 1500000;
      const privateKey = private_key;
      const merchantCode = merchant_code;
      const merchantRef = merchant_ref;
      const amount = amountOf;

      const data = `${merchantCode}${merchantRef}${amount}`;
      const signature = crypto
        .createHmac('sha256', privateKey)
        .update(data)
        .digest('hex');
      return signature;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: `Exeption Handler Looging for ${err.message}`,
        err: err,
        errCustomCode: '20',
      });
      throw new Error(`Failed to create Signature : ${err.message}`);
    }
  }
  private async getExpiredTimeEpoch(): Promise<number> {
    const currentTime = Math.floor(new Date().getTime() / 1000);
    const TimeInMilliseconds = 24 * 60 * 60 * 1000;
    const expirationTimeInMilliseconds = currentTime + TimeInMilliseconds;
    return expirationTimeInMilliseconds;
  }
}
