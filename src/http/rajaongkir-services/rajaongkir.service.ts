import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import axios from 'axios';

@Injectable()
export class RajaongkirServices {
  private logger = new Logger(RajaongkirServices.name);
  constructor(private readonly config: ConfigService) {}

  async getAllProvinsi() {
    const apiUrl = this.config.get<string>('ONGKIR_GATEWAY');
    const apiKey = this.config.get<string>('RAJAONGKIR_APIKEY');
    const url = `${apiUrl}province`;
    try {
      const getProvince = await axios.get(url, {
        headers: { key: apiKey },
      });
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull Get Data',
          status_code: 200,
        },
        data: getProvince.data.rajaongkir.results,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: `Exeption Handler Looging for ${err.message}`,
        err: err,
        errCustomCode: '20',
      });
      throw new Error(`Failed to fetch data from ${url}: ${err.message}`);
    }
  }
}
