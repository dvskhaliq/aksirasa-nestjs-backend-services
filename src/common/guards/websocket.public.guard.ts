import { Injectable, CanActivate } from '@nestjs/common';

@Injectable()
export class WebsocketPublicGuard implements CanActivate {
  canActivate(): boolean {
    // Izinkan semua koneksi
    return true;
  }
}
