import {
  Injectable,
  CanActivate,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';

@Injectable()
export class WebsocketAuthGuard implements CanActivate {
  constructor(
    private readonly jwtService: JwtService,
    private config: ConfigService,
  ) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const client = context.switchToWs().getClient();
    const token = client.handshake.headers.authorization?.replace(
      'Bearer ',
      '',
    );
    if (!token) {
      throw new UnauthorizedException('Token not provided');
    }
    try {
      const decoded = this.jwtService.verify(token);
      // Set informasi pengguna ke dalam request untuk penggunaan lebih lanjut
      const request = context.switchToHttp().getRequest();
      const user = {
        userId: decoded.sub,
        issuedAt: new Date(decoded.iat * 1000),
        expiresAt: new Date(decoded.exp * 1000),
      };
      request.user = user;
      // Lanjutkan ke middleware berikutnya
      return true;
    } catch (err) {
      throw new UnauthorizedException('Invalid token');
    }
  }
}
