import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  BadRequestException,
} from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class LimitFilesInterceptor implements NestInterceptor {
  constructor(private readonly maxFiles: number) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const http = context.switchToHttp();
    const request = http.getRequest();

    if (!request.files || request.files.length === 0) {
      throw new BadRequestException('No files uploaded');
    }

    if (
      Array.isArray(request.files.image) &&
      request.files.image.length > this.maxFiles
    ) {
      throw new BadRequestException(
        `Maximum ${this.maxFiles} files are allowed`,
      );
    }

    return next.handle();
  }
}
