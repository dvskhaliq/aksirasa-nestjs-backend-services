// transform.interceptor.ts
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  HttpStatus,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class TransformInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map((data) => {
        const status = data.message.status_code;
        // Lakukan transformasi pada data respons di sini
        if (status === HttpStatus.OK) {
          const transformedData = {
            status: data.message,
            data: data.data, // Data respons asli dari metode handler
          };
          return transformedData;
        } else if (status === HttpStatus.NO_CONTENT) {
          return {};
        } else if (status === HttpStatus.CREATED) {
          const transformedData = {
            status: data.message,
          };
          return transformedData;
        } else {
          return { success: false, data };
        }
      }),
    );
  }
}
