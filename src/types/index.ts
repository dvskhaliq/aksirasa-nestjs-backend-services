export type StandardResponse = {
  execution: boolean;
  stack: string;
  message: {
    message: string;
    status_code: number;
  };
  data: object;
};
