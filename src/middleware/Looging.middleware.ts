// logging.middleware.ts

import { Injectable, Logger, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { AppService } from 'src/app.service';

@Injectable()
export class LoggingMiddleware implements NestMiddleware {
  private logger = new Logger(AppService.name);
  use(req: Request, res: Response, next: NextFunction) {
    const start = Date.now();
    // Tangkap event saat request masuk
    this.logger.log({
      timestamp: start,
      level: 'info',
      method: req.method,
      message: 'Incooming Request',
      data: req.body,
      param: req.params,
    });

    // Lanjutkan ke handler selanjutnya
    next();

    // Tangkap event saat response keluar
    res.on('finish', () => {
      const duration = Date.now() - start;
      this.logger.log({
        timestamp: duration,
        level: 'info',
        status_code: res.statusCode,
        message: res.statusMessage,
      });
    });
  }
}
