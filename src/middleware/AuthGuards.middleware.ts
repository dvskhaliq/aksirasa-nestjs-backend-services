import {
  Injectable,
  Logger,
  NestMiddleware,
  UnauthorizedException,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { OrganizationService } from '../services/organization/organization.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthGuardsMiddleware implements NestMiddleware {
  private logger = new Logger(OrganizationService.name);
  constructor(private readonly jwtService: JwtService) {}

  use(req: Request, res: Response, next: NextFunction) {
    const token = req.headers.authorization?.replace('Bearer ', '');
    if (token) {
      try {
        const decoded = this.jwtService.verify(token);
        // Set informasi pengguna ke dalam request untuk penggunaan lebih lanjut
        req['user'] = {
          userId: decoded.sub,
          issuedAt: new Date(decoded.iat * 1000),
          expiresAt: new Date(decoded.exp * 1000),
        };
        // Lanjutkan ke middleware berikutnya
        next();
      } catch (err) {
        throw new UnauthorizedException('Invalid token');
      }
    } else {
      // Token tidak tersedia
      throw new UnauthorizedException('Token not provided');
    }
  }
}
