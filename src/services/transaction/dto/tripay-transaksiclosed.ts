import { Type } from 'class-transformer';
import {
  IsArray,
  IsInt,
  // IsInt,
  IsNotEmpty,
  IsNumberString,
  IsString,
  ValidateNested,
  // ValidateNested,
} from 'class-validator';

export class order_items {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsInt()
  @IsNotEmpty()
  quantity: number;

  @IsNumberString()
  @IsNotEmpty()
  price: number;
}
export class TransactionClosedDto {
  @IsString()
  @IsNotEmpty()
  metode_pembayaran: string;

  @IsString()
  @IsNotEmpty()
  merchan_ref: string;

  @IsNumberString()
  @IsNotEmpty()
  total_pembayaran: number;

  @IsString()
  @IsNotEmpty()
  customer_name: string;

  @IsString()
  @IsNotEmpty()
  customer_email: string;

  @IsNumberString()
  @IsNotEmpty()
  customer_phone: string;

  @IsArray()
  @IsNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => order_items)
  order_items: order_items[];

  @IsString()
  @IsNotEmpty()
  callback_url: string;
}
