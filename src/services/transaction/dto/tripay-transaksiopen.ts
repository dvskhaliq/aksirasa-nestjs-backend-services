import { IsNotEmpty, IsString } from 'class-validator';

export class TransactionOpenDto {
  @IsString()
  @IsNotEmpty()
  metode_pembayaran: string;

  @IsString()
  @IsNotEmpty()
  kode_transaksi: string;

  @IsString()
  @IsNotEmpty()
  customer_name: string;

  @IsString()
  @IsNotEmpty()
  signature: string;
}
