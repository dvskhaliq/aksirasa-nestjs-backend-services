import { Type } from 'class-transformer';
import {
  IsArray,
  IsEmpty,
  IsInt,
  IsNotEmpty,
  IsNumberString,
  IsString,
  ValidateNested,
} from 'class-validator';

export class order_Items {
  @IsString()
  @IsNotEmpty()
  yayasan_id: string;

  @IsString()
  @IsNotEmpty()
  nama_product: string;

  @IsInt()
  @IsNotEmpty()
  qty: number;

  @IsNumberString()
  @IsNotEmpty()
  harga: number;
}

export class TransactionDonationDto {
  @IsString()
  @IsNotEmpty()
  nama_lengkap: string;

  @IsNumberString()
  @IsNotEmpty()
  no_hp: string;

  @IsArray()
  @IsNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => order_Items)
  order_items: order_Items[];

  @IsString()
  @IsEmpty()
  notes: string;
}
