import { Type } from 'class-transformer';
import {
  IsArray,
  IsEmpty,
  IsInt,
  IsNotEmpty,
  IsNumberString,
  IsString,
  ValidateNested,
} from 'class-validator';

export class orderItems {
  @IsString()
  @IsNotEmpty()
  product_id: string;

  @IsString()
  @IsNotEmpty()
  nama_product: string;

  @IsInt()
  @IsNotEmpty()
  qty: number;

  @IsNumberString()
  @IsNotEmpty()
  harga: number;
}

export class CreateTransactionDto {
  @IsString()
  @IsNotEmpty()
  yayasan_id: string;

  @IsString()
  @IsNotEmpty()
  nama_lengkap: string;

  @IsNumberString()
  @IsNotEmpty()
  no_hp: string;

  @IsString()
  @IsNotEmpty()
  alamat_pengirim: string;

  @IsArray()
  @IsNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => orderItems)
  order_items: orderItems[];

  @IsString()
  @IsEmpty()
  notes: string;
}
