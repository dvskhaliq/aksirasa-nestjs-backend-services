import {
  Controller,
  Get,
  Post,
  Body,
  // Patch,
  Param,
  // Delete,
  Version,
  UseGuards,
  Req,
  HttpException,
} from '@nestjs/common';
import { TransactionService } from './transaction.service';
import {
  CreateTransactionDto,
  TransactionOpenDto,
  TransactionClosedDto,
  TransactionDonationDto,
} from './dto/index';
import { TripayService } from '../../http/tripay-services/tripay.service';
// import { AuthGuardsMiddleware } from 'src/middleware/AuthGuards.middleware';
import { PublicGuard } from 'src/common/guards/public.guard';
import { Request } from 'express';
import { AuthGuard } from 'src/common/guards/auth.guard';

@Controller('transaction')
export class TransactionController {
  constructor(
    private readonly transactionService: TransactionService,
    private readonly tripayService: TripayService,
  ) {}

  //GET Controller
  @Version('1')
  @Get('/detail/:transaction_id')
  @UseGuards(AuthGuard)
  async getTransactionDetail(@Param('transaction_id') transaction_id: string) {
    const getTransaction =
      await this.transactionService.getTransactionDetail(transaction_id);
    if (getTransaction.execution === true && getTransaction.stack === 'done') {
      const getTripay = await this.getDetailTransaksiTripay(
        getTransaction.data.reference_code,
      );
      const data = { ...getTransaction.data, ...getTripay };
      const response = {
        message: {
          message: getTransaction.message.message,
          status_code: getTransaction.message.status_code,
        },
        data: data,
      };
      return response;
    } else {
      throw new HttpException(
        getTransaction.message.message,
        getTransaction.message.status_code,
      );
    }
  }
  @Version('1')
  @Get('/tripay/payment/intruction/:code')
  @UseGuards(AuthGuard)
  async getIntruksiPembayaran(@Param('code') code_va: string) {
    const tripayGet = await this.tripayService.getIntruksiPembayaran(code_va);
    if (tripayGet.execution === true && tripayGet.stack === 'done') {
      return tripayGet;
    } else {
      throw new HttpException(
        tripayGet.message.message,
        tripayGet.message.status_code,
      );
    }
  }
  @Version('1')
  @Get('/tripay/payment-channel')
  @UseGuards(AuthGuard)
  async getPaymentChannel() {
    const getPaymentChannel = await this.tripayService.getPaymentChannel();
    if (
      getPaymentChannel.execution === true &&
      getPaymentChannel.stack === 'done'
    ) {
      return getPaymentChannel;
    } else {
      throw new HttpException(
        getPaymentChannel.message.message,
        getPaymentChannel.message.status_code,
      );
    }
  }
  //Post Controller
  @Version('1')
  @Post('/tripay/payment/closed')
  @UseGuards(AuthGuard)
  async postRequestTransaksiClosed(
    @Body() transactionClosedDto: TransactionClosedDto,
  ) {
    const requestTransactionClosed =
      await this.tripayService.requestTransaksiClosed(transactionClosedDto);
    if (
      requestTransactionClosed.execution === true &&
      requestTransactionClosed.stack === 'done'
    ) {
      const reference_code = requestTransactionClosed.data.reference;
      const merchan_ref = requestTransactionClosed.data.merchant_ref;
      const putReference = await this.updateTransaksiCloseTripay(
        merchan_ref,
        reference_code,
      );
      return putReference;
    } else {
      throw new HttpException(
        requestTransactionClosed.message.message,
        requestTransactionClosed.message.status_code,
      );
    }
  }
  @Version('1')
  @Post('/tripay/payment/open')
  @UseGuards(AuthGuard)
  async postRequestTransaksiOpen(
    @Body() RequestTransactionOpenDto: TransactionOpenDto,
  ) {
    const requestOpenTransaksi = await this.tripayService.requestTransaksiOpen(
      RequestTransactionOpenDto,
    );
    if (
      requestOpenTransaksi.execution === true &&
      requestOpenTransaksi.stack === 'done'
    ) {
      return requestOpenTransaksi;
    } else {
      throw new HttpException(
        requestOpenTransaksi.message.message,
        requestOpenTransaksi.message.status_code,
      );
    }
  }
  @Version('1')
  @UseGuards(PublicGuard)
  @Post('/tripay/callback_url')
  async callback_url(@Body() data: any) {
    console.log('Received callback data:', data);
  }

  @Version('1')
  @Post('/donation/post')
  @UseGuards(AuthGuard)
  async postDonation(
    @Req() req: Request,
    @Body() donationDto: TransactionDonationDto,
  ) {
    const userData = req['user'];
    const postDonations = await this.transactionService.postTransctionDonation(
      userData,
      donationDto,
    );
    if (postDonations.execution === true && postDonations.stack === 'done') {
      return postDonations;
    } else {
      throw new HttpException(
        postDonations.message.message,
        postDonations.message.status_code,
      );
    }
  }
  @Version('1')
  @Post('/product/post')
  @UseGuards(AuthGuard)
  async postTransaction(
    @Req() req: Request,
    @Body() transactionDto: CreateTransactionDto,
  ) {
    const userData = req['user'];
    const postTransaction = await this.transactionService.postTransction(
      userData,
      transactionDto,
    );
    if (
      postTransaction.execution === true &&
      postTransaction.stack === 'done'
    ) {
      return postTransaction;
    } else {
      throw new HttpException(
        postTransaction.message.message,
        postTransaction.message.status_code,
      );
    }
  }
  private async getDetailTransaksiTripay(reference_code: string) {
    const getDetailTransaksi =
      await this.tripayService.getDetailTrasaksi(reference_code);
    if (
      getDetailTransaksi.execution === true &&
      getDetailTransaksi.stack === 'done'
    ) {
      const response = {
        transaction: {
          reference: getDetailTransaksi.data.reference,
          merchant_ref: getDetailTransaksi.data.merchan_ref,
          payment_method: getDetailTransaksi.data.payment_method,
          payment_name: getDetailTransaksi.data.payment_name,
          customer_name: getDetailTransaksi.data.customer_name,
          customer_email: getDetailTransaksi.data.customer_email,
          customer_phone: getDetailTransaksi.data.customer_phone,
          callback_url: getDetailTransaksi.data.callback_url,
          return_url: getDetailTransaksi.data.return_url,
          amount: getDetailTransaksi.data.amount,
          fee_merchant: getDetailTransaksi.data.fee_merchant,
          fee_customer: getDetailTransaksi.data.fee_customer,
          total_fee: getDetailTransaksi.data.total_fee,
          pay_code: getDetailTransaksi.data.pay_code,
          pay_url: getDetailTransaksi.data.pay_url,
          status_payment: getDetailTransaksi.data.status,
          paid_at: getDetailTransaksi.data.paid_at,
          expired_time: getDetailTransaksi.data.expired_time,
        },
      };
      return response;
    } else {
      throw new HttpException(
        getDetailTransaksi.message.message,
        getDetailTransaksi.message.status_code,
      );
    }
  }

  private async updateTransaksiCloseTripay(
    transaction_code: string,
    reference_code: string,
  ) {
    const putTransaksi = await this.transactionService.putReferenceCode(
      transaction_code,
      reference_code,
    );
    if (putTransaksi.execution === true && putTransaksi.stack === 'done') {
      return putTransaksi;
    } else {
      throw new HttpException(
        putTransaksi.message.message,
        putTransaksi.message.status_code,
      );
    }
  }
  // @Post()
  // create(@Body() createTransactionDto: CreateTransactionDto) {
  //   return this.transactionService.create(createTransactionDto);
  // }

  // @Get()
  // findAll() {
  //   return this.transactionService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.transactionService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateTransactionDto: UpdateTransactionDto) {
  //   return this.transactionService.update(+id, updateTransactionDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.transactionService.remove(+id);
  // }
}
