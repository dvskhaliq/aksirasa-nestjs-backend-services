import { Injectable, Logger } from '@nestjs/common';
import { CreateTransactionDto, TransactionDonationDto } from './dto/index';
import { PrismaService } from 'src/prisma/prisma.service';
import { ConfigService } from '@nestjs/config';
import { PrismaClientExceptionFilter } from 'src/common/prisma-client-exception/prisma-client-exception.filter';
import cuid = require('cuid');

@Injectable()
export class TransactionService {
  private logger = new Logger(TransactionService.name);
  constructor(
    private prisma: PrismaService,
    private config: ConfigService,
  ) {}

  async getTransactionDetail(transaction_id: string) {
    try {
      const getTransactionDetail =
        await this.prisma.transaction_product.findFirst({
          where: { id: transaction_id },
        });
      if (getTransactionDetail != null) {
        const getOrderItem = await this.prisma.order_item.findMany({
          where: { transaction_id: transaction_id },
        });
        if (getOrderItem.length < 1) {
          const response = {
            execution: false,
            stack: 'transaction',
            message: {
              message: 'No Data Found',
              status_code: 404,
            },
            data: null,
          };
          return response;
        }
        const data = {
          id: getTransactionDetail.id,
          reference_code: getTransactionDetail.references_code,
          kode_transaksi: getTransactionDetail.kode_transaksi,
          fullname: getTransactionDetail.fullname,
          alamat_pengirim: getTransactionDetail.alamat_pengirim,
          notes: getTransactionDetail.notes,
          status_pembayaran: getTransactionDetail.status_pembayaran,
          order_items: getOrderItem,
        };
        const response = {
          execution: true,
          stack: 'done',
          message: {
            message: 'Successfull find data',
            status_code: 200,
          },
          data: data,
        };
        return response;
      } else {
        const response = {
          execution: false,
          stack: 'transaction',
          message: {
            message: 'No Data Found',
            status_code: 404,
          },
          data: null,
        };
        return response;
      }
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async postTransction(userData: any, transactionDto: CreateTransactionDto) {
    try {
      const transactionCode = await this.generateTransactionCode();
      const myCuid = await this.generateCuid();
      const postTransaction = await this.prisma.transaction_product.create({
        data: {
          id: myCuid,
          user_id: userData.userId,
          kode_transaksi: transactionCode,
          fullname: transactionDto.nama_lengkap,
          phone: transactionDto.no_hp,
          alamat_pengirim: transactionDto.alamat_pengirim,
          notes: transactionDto.notes,
        },
      });
      if (!postTransaction) {
        const response = {
          execution: false,
          stack: 'Register',
          message: {
            message: 'Failed to sent trasaction',
            status_code: 500,
          },
          data: {},
        };
        return response;
      }
      await this.createOrderItems(myCuid, transactionDto.order_items);
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull create product',
          status_code: 200,
        },
        data: {},
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }

  async postTransctionDonation(
    userData: any,
    donationDto: TransactionDonationDto,
  ) {
    try {
      const kodeTransaksi = await this.generateTransactionCode();
      const myCuid = await this.generateCuid();
      const postDonations = await this.prisma.transaction_product.create({
        data: {
          id: myCuid,
          user_id: userData.userId,
          kode_transaksi: kodeTransaksi,
          fullname: donationDto.nama_lengkap,
          phone: donationDto.no_hp,
          notes: donationDto.notes,
        },
      });
      if (!postDonations) {
        const response = {
          execution: false,
          stack: 'Register',
          message: {
            message: 'Failed to sent trasaction',
            status_code: 500,
          },
          data: {},
        };
        return response;
      }
      await this.createItemsDonation(myCuid, donationDto.order_items);
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull create product',
          status_code: 200,
        },
        data: {},
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async putReferenceCode(transaction_code: string, reference_code: string) {
    try {
      const putReference = await this.prisma.transaction_product.update({
        where: { kode_transaksi: transaction_code },
        data: {
          references_code: reference_code,
        },
      });
      if (!putReference) {
        const response = {
          execution: false,
          stack: 'update references',
          message: {
            message: 'Failed to update trasaction',
            status_code: 500,
          },
          data: {},
        };
        return response;
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull update transaction',
          status_code: 200,
        },
        data: {},
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async generateCuid(): Promise<string> {
    const Mycuid = cuid();
    return Mycuid;
  }
  async generateTransactionCode(): Promise<string> {
    const min = 100000; // Angka terkecil dengan 6 digit
    const max = 999999; // Angka terbesar dengan 6 digit
    const randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;
    const transactionCode = `TRC${randomNumber}`;
    return transactionCode;
  }
  private async createOrderItems(
    transactionId: string,
    order_items: any,
  ): Promise<void> {
    try {
      const createOrderItems = order_items.map((item) => {
        return this.prisma.order_item.create({
          data: {
            product_id: item.product_id,
            transaction_id: transactionId,
            nama_product: item.nama_product,
            qty: item.qty,
            harga: item.harga,
          },
        });
      });
      await Promise.all(createOrderItems);
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }

  private async createItemsDonation(
    transactionId: string,
    order_items: any,
  ): Promise<void> {
    try {
      console.log({ order_items: order_items });
      const createOrderItems = order_items.map((item) => {
        return this.prisma.order_item.create({
          data: {
            yayasan_id: item.yayasan_id,
            transaction_id: transactionId,
            nama_product: item.nama_product,
            qty: item.qty,
            harga: item.harga,
          },
        });
      });
      await Promise.all(createOrderItems);
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
}
// private async postOrderItems(
//   transaction_id: string,
//   order_items: orderItems,
// ) {
//   try {
//     const postOrderItems = await this.prisma.order_item.create({
//       data: {
//         transaction_id: transaction_id,
//         nama_product: order_items.nama_product,
//         qty: order_items.qty,
//         harga: order_items.harga,
//       },
//     });
//     return postOrderItems;
//   } catch (err) {
//     this.logger.log({
//       level: 'error',
//       message: 'Exeption Handler Looging for',
//       err: err,
//       errCustomCode: '20',
//     });
//     throw new PrismaClientExceptionFilter();
//   }
// }
// create(createTransactionDto: CreateTransactionDto) {
//   return 'This action adds a new transaction';
// }

// findAll() {
//   return `This action returns all transaction`;
// }

// findOne(id: number) {
//   return `This action returns a #${id} transaction`;
// }

// update(id: number, updateTransactionDto: UpdateTransactionDto) {
//   return `This action updates a #${id} transaction`;
// }

// remove(id: number) {
//   return `This action removes a #${id} transaction`;
// }
// }
