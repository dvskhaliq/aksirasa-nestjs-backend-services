import { IsNotEmpty, IsNumberString, IsString } from 'class-validator';

export class RegisterAktivitasDto {
  @IsNotEmpty()
  @IsString()
  berita_acara: string;

  @IsNotEmpty()
  @IsString()
  judul_berita: string;

  @IsNotEmpty()
  @IsString()
  lokasi_alamat: string;

  @IsNotEmpty()
  @IsString()
  lokasi_kelurahan: string;

  @IsNotEmpty()
  @IsString()
  lokasi_kecamatan: string;

  @IsNotEmpty()
  @IsString()
  lokasi_provinsi: string;
}

export class QueryParamAktivitasALL {
  @IsNotEmpty()
  @IsNumberString()
  page: string;

  @IsNotEmpty()
  @IsNumberString()
  take: string;

  @IsNotEmpty()
  @IsString()
  order: string;
}
