// upload-files.dto.ts

import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty } from 'class-validator';
import { Express } from 'express';

export class UploadFilesDto {
  @ApiProperty({
    type: 'array',
    items: {
      type: 'string',
      format: 'binary',
    },
  })
  @IsArray()
  @IsNotEmpty({ message: 'At least one file should be provided.' })
  files: Express.Multer.File[];
}
