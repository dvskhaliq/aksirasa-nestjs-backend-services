import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsNotEmpty,
  IsNumberString,
  IsString,
  Length,
} from 'class-validator';

export class RegisterOrganizationDto {
  @IsNotEmpty()
  @IsString()
  fullname_organisasi: string;

  @IsNotEmpty()
  @IsString()
  @Length(8, 16, {
    message: 'Nomer handphone harus memiliki panjang maksimal 16 digit',
  })
  phone: string;

  @IsNotEmpty()
  @IsString()
  @Length(10, 16, {
    message:
      'Nomer Rekening harus memiliki panjang minimal 10 digit dan maksimal 16 digit ',
  })
  norek: string;

  @IsNotEmpty()
  @IsString()
  atas_nama: string;

  @IsNotEmpty()
  @IsString()
  nama_bank: string;

  @IsNotEmpty()
  @IsString()
  provinsi: string;

  @IsString()
  @Length(5, 100, {
    message: 'alamat tidak boleh melebihi 100 karakter',
  })
  alamat: string;

  @IsString()
  @Length(5, 255, {
    message: 'Tentang Organisasi tidak boleh melebihi 225 karakter',
  })
  tentang_organisasi: string;

  @IsString()
  @Length(5, 25, {
    message: 'link website harus memiliki panjang maksimal 25 karakter',
  })
  website: string;

  @IsNotEmpty()
  @IsString()
  @Length(5, 20, {
    message: 'Nomor izin harus memiliki panjang maksimal 20 karakter',
  })
  nomer_izin: string;

  @IsString()
  @Length(5, 25, {
    message: 'link social media harus memiliki panjang maksimal 25 karakter',
  })
  link_fb: string;

  @IsString()
  @Length(5, 25, {
    message: 'link social media harus memiliki panjang maksimal 25 karakter',
  })
  link_instagram: string;

  @IsString()
  @Length(5, 25, {
    message: 'link social media harus memiliki panjang maksimal 25 karakter',
  })
  link_twitter: string;
}

export class UpdateOrganizationDto {
  @IsString()
  fullname_organisasi: string;

  @IsString()
  phone: string;

  @IsString()
  norek: string;

  @IsString()
  atas_nama: string;

  @IsString()
  nama_bank: string;

  @IsString()
  alamat: string;

  @IsString()
  tentang_organisasi: string;

  @IsString()
  website: string;

  @IsString()
  nomer_izin: string;

  @IsString()
  link_fb: string;

  @IsString()
  link_instagram: string;

  @IsString()
  link_twitter: string;
}

export class findOrganizationParamDto {
  @IsString()
  keyword: string;

  @IsArray()
  provinsi: string[];
}

export class QueryParamOrganizationALL {
  @IsNotEmpty()
  @IsNumberString()
  page: string;

  @IsNotEmpty()
  @IsNumberString()
  take: string;

  @IsNotEmpty()
  @IsString()
  order: string;
}

export class UploadFilesDto {
  @ApiProperty({
    type: 'array',
    items: {
      type: 'string',
      format: 'binary',
    },
  })
  @IsArray()
  @IsNotEmpty({ message: 'At least one file should be provided.' })
  files: Express.Multer.File[];
}
