import { Module } from '@nestjs/common';
import { OrganizationService } from './organization.service';
import { OrganizationController } from './organization.controller';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [JwtModule], // ... modul lainnya,
  controllers: [OrganizationController],
  providers: [OrganizationService],
})
export class OrganizationModule {}
