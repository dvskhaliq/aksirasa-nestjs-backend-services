import {
  BadRequestException,
  Injectable,
  Logger,
  HttpStatus,
} from '@nestjs/common';
import {
  RegisterOrganizationDto,
  RegisterAktivitasDto,
  UpdateOrganizationDto,
} from './dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { ConfigService } from '@nestjs/config';
import { PrismaClientExceptionFilter } from 'src/common/prisma-client-exception/prisma-client-exception.filter';
import cuid = require('cuid');
import path = require('path');
import * as fs from 'fs';
import { StandardResponse } from 'src/types';
// import { AnyARecord } from 'dns';

type ImgListParams = {
  aktivitas_id: string;
  user_id: string;
  yayasan_id: string;
  command: string;
};

@Injectable()
export class OrganizationService {
  private logger = new Logger(OrganizationService.name);
  constructor(
    private prisma: PrismaService,
    private config: ConfigService,
  ) {}
  async getTotalItem() {
    try {
      const getCatalog = await this.prisma.yayasan.count();
      if (!getCatalog) {
        const response = {
          execution: false,
          stack: 'getCatalog',
          message: {
            message: 'No data found',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }
      const total = getCatalog;
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull found data',
          status_code: 200,
        },
        data: total,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async getTotalActivity(yayasan_id?: string) {
    try {
      if (yayasan_id && yayasan_id != null) {
        const getCatalog = await this.prisma.aktivitas_organisasi.count({
          where: { yayasan_id: yayasan_id },
        });
        if (!getCatalog) {
          const response = {
            execution: false,
            stack: 'getCatalog',
            message: {
              message: 'No data found',
              status_code: 404,
            },
            data: {},
          };
          return response;
        }
        const total = getCatalog;
        const response = {
          execution: true,
          stack: 'done',
          message: {
            message: 'Sucessfull found data',
            status_code: 200,
          },
          data: total,
        };
        return response;
      } else {
        const getCatalog = await this.prisma.aktivitas_organisasi.count();
        if (!getCatalog) {
          const response = {
            execution: false,
            stack: 'getCatalog',
            message: {
              message: 'No data found',
              status_code: 404,
            },
            data: {},
          };
          return response;
        }
        const total = getCatalog;
        const response = {
          execution: true,
          stack: 'done',
          message: {
            message: 'Sucessfull found data',
            status_code: 200,
          },
          data: total,
        };
        return response;
      }
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async registerOrg(
    RegistOrganizationDto: RegisterOrganizationDto,
    userData: any,
    file: Express.Multer.File,
  ): Promise<StandardResponse> {
    try {
      //check user if any register have organization
      const valid = await this.prisma.yayasan.count({
        where: { user_id: userData.userId },
      });
      if (valid > 0) {
        const response = {
          execution: false,
          stack: 'Register',
          message: {
            message:
              'Request Failed !!, Satu User hanya boleh mendaftarkan 1 Yayasan',
            status_code: 400,
          },
          data: {},
        };
        return response;
      }
      const myCuid = cuid();
      const myDate = new Date(); // Your timezone!
      const myEpoch = Math.floor(myDate.getTime() / 1000.0);
      const register = await this.prisma.yayasan.create({
        data: {
          id: myCuid,
          user_id: userData.userId,
          fullname_organisasi: RegistOrganizationDto.fullname_organisasi,
          phone: RegistOrganizationDto.phone,
          no_rek: RegistOrganizationDto.norek, //troubleshoot issue
          atas_nama: RegistOrganizationDto.atas_nama,
          nama_bank: RegistOrganizationDto.nama_bank,
          provinsi: RegistOrganizationDto.provinsi,
          alamat: RegistOrganizationDto.alamat,
          tentang_organisasi: RegistOrganizationDto.tentang_organisasi,
          nomer_izin: RegistOrganizationDto.nomer_izin,
          website: RegistOrganizationDto.website,
          link_fb: RegistOrganizationDto.link_fb,
          link_instagram: RegistOrganizationDto.link_instagram,
          link_twitter: RegistOrganizationDto.link_twitter,
          tanggal_post: myEpoch,
        },
      });
      console.log(register);

      if (!register) {
        const response = {
          execution: false,
          stack: 'Register',
          message: {
            message: 'Request Failed !!',
            status_code: 500,
          },
          data: {},
        };
        return response;
      }

      const save_berkas = await this.save_berkas(myCuid, file);

      if (!save_berkas) {
        const response = {
          execution: false,
          stack: 'Save Berkas',
          message: {
            message: 'Failed to upload your file',
            status_code: 400,
          },
          data: {},
        };
        return response;
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Success register your organization',
          status_code: 201,
        },
        data: {},
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async findOrganizationAll(
    skip: number,
    take: number,
    order: boolean,
  ): Promise<StandardResponse> {
    try {
      if (order === false) {
        const yayasan = await this.prisma.yayasan.findMany({
          skip: skip,
          take: take,
        });
        if (!yayasan) {
          const response = {
            execution: false,
            stack: 'getOrg',
            message: {
              message: 'Failed to find data',
              status_code: 400,
            },
            data: {},
          };
          return response;
        }
        const data: object[] = [];
        for (let index = 0; index < yayasan.length; index++) {
          const findCover = await this.findIMGCoverORG(yayasan[index].id);
          const response = {
            id: yayasan[index].id,
            user_id: yayasan[index].user_id,
            fullname_organisasi: yayasan[index].fullname_organisasi,
            phone: yayasan[index].phone,
            no_rek: yayasan[index].no_rek,
            atas_name: yayasan[index].atas_nama,
            provinsi: yayasan[index].provinsi,
            alamat: yayasan[index].alamat,
            tentang_organisasi: yayasan[index].tentang_organisasi,
            website: yayasan[index].website,
            nomer_izin: yayasan[index].nomer_izin,
            link_fb: yayasan[index].link_fb,
            link_isntagram: yayasan[index].link_instagram,
            link_twitter: yayasan[index].link_twitter,
            tanggal_post: yayasan[index].tanggal_post,
            image: findCover,
          };
          data.push(response);
        }
        const response = {
          execution: true,
          stack: 'done',
          message: {
            message: 'Generate sucessfull',
            status_code: 200,
          },
          data: data,
        };
        return response;
      } else {
        const yayasan = await this.prisma.yayasan.findMany({
          skip: skip,
          take: take,
          orderBy: {
            tanggal_post: 'desc', // Ganti 'tanggal' dengan nama kolom yang sesuai dalam tabel Anda
          },
        });
        if (!yayasan) {
          const response = {
            execution: false,
            stack: 'getOrg',
            message: {
              message: 'Failed to find data',
              status_code: 400,
            },
            data: {},
          };
          return response;
        }
        const data: object[] = [];
        for (let index = 0; index < yayasan.length; index++) {
          const findCover = await this.findIMGCoverORG(yayasan[index].id);
          const response = {
            id: yayasan[index].id,
            user_id: yayasan[index].user_id,
            fullname_organisasi: yayasan[index].fullname_organisasi,
            phone: yayasan[index].phone,
            no_rek: yayasan[index].no_rek,
            atas_name: yayasan[index].atas_nama,
            provinsi: yayasan[index].provinsi,
            alamat: yayasan[index].alamat,
            tentang_organisasi: yayasan[index].tentang_organisasi,
            website: yayasan[index].website,
            nomer_izin: yayasan[index].nomer_izin,
            link_fb: yayasan[index].link_fb,
            link_isntagram: yayasan[index].link_instagram,
            link_twitter: yayasan[index].link_twitter,
            tanggal_post: yayasan[index].tanggal_post,
            image: findCover,
          };
          data.push(response);
        }
        const response = {
          execution: true,
          stack: 'done',
          message: {
            message: 'Generate sucessfull',
            status_code: 200,
          },
          data: data,
        };
        return response;
      }
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async findActivityAll(
    skip: number,
    take: number,
    order: boolean,
    yayasan_id?: string,
  ): Promise<StandardResponse> {
    try {
      if (order === false) {
        if (yayasan_id && yayasan_id !== '') {
          const activity = await this.prisma.aktivitas_organisasi.findMany({
            skip: skip,
            take: take,
            where: { yayasan_id: yayasan_id },
          });
          if (!activity) {
            const response = {
              execution: false,
              stack: 'getActivity',
              message: {
                message: 'Failed to find data',
                status_code: 500,
              },
              data: {},
            };
            return response;
          }
          const data: object[] = [];
          for (const activity_list of activity) {
            const processImage = await this.findIMGAktivitasORG(
              activity_list.id,
            );
            const Owner = await this.findOwnerProduct(activity_list.yayasan_id);
            const processedData = {
              id: activity_list.id,
              judul_berita: activity_list.judul_berita,
              berita_acara: activity_list.berita_acara,
              lokasi_alamat: activity_list.lokasi_alamat,
              lokasi_kelurahan: activity_list.lokasi_kelurahan,
              lokasi_kecamatan: activity_list.lokasi_kecamatan,
              lokasi_provinsi: activity_list.lokasi_provinsi,
              tanggal_post: activity_list.tanggal_post,
              image: processImage,
              owner: Owner,
            };
            data.push(processedData);
          }
          const response = {
            execution: true,
            stack: 'done',
            message: {
              message: 'Generate sucessfull',
              status_code: 200,
            },
            data: data,
          };
          return response;
        } else {
          const activity = await this.prisma.aktivitas_organisasi.findMany({
            skip: skip,
            take: take,
          });
          if (!activity) {
            const response = {
              execution: false,
              stack: 'getActivity',
              message: {
                message: 'Failed to find data',
                status_code: 500,
              },
              data: {},
            };
            return response;
          }
          const data: object[] = [];
          for (const activity_list of activity) {
            const processImage = await this.findIMGAktivitasORG(
              activity_list.id,
            );
            const Owner = await this.findOwnerProduct(activity_list.yayasan_id);
            const processedData = {
              id: activity_list.id,
              judul_berita: activity_list.judul_berita,
              berita_acara: activity_list.berita_acara,
              lokasi_alamat: activity_list.lokasi_alamat,
              lokasi_kelurahan: activity_list.lokasi_kelurahan,
              lokasi_kecamatan: activity_list.lokasi_kecamatan,
              lokasi_provinsi: activity_list.lokasi_provinsi,
              tanggal_post: activity_list.tanggal_post,
              image: processImage,
              owner: Owner,
            };
            data.push(processedData);
          }
          const response = {
            execution: true,
            stack: 'done',
            message: {
              message: 'Generate sucessfull',
              status_code: 200,
            },
            data: data,
          };
          return response;
        }
      } else {
        if (yayasan_id && yayasan_id !== '') {
          const activity = await this.prisma.aktivitas_organisasi.findMany({
            skip: skip,
            take: take,
            where: { yayasan_id: yayasan_id },
            orderBy: {
              tanggal_post: 'desc',
            },
          });
          if (!activity) {
            const response = {
              execution: false,
              stack: 'getActivity',
              message: {
                message: 'Failed to find data',
                status_code: 500,
              },
              data: {},
            };
            return response;
          }
          const data: object[] = [];
          for (const activity_list of activity) {
            const processImage = await this.findIMGAktivitasORG(
              activity_list.id,
            );
            const Owner = await this.findOwnerProduct(activity_list.yayasan_id);
            const processedData = {
              id: activity_list.id,
              judul_berita: activity_list.judul_berita,
              berita_acara: activity_list.berita_acara,
              lokasi_alamat: activity_list.lokasi_alamat,
              lokasi_kelurahan: activity_list.lokasi_kelurahan,
              lokasi_kecamatan: activity_list.lokasi_kecamatan,
              lokasi_provinsi: activity_list.lokasi_provinsi,
              tanggal_post: activity_list.tanggal_post,
              image: processImage,
              owner: Owner,
            };
            data.push(processedData);
          }
          const response = {
            execution: true,
            stack: 'done',
            message: {
              message: 'Generate sucessfull',
              status_code: 200,
            },
            data: data,
          };
          return response;
        } else {
          const activity = await this.prisma.aktivitas_organisasi.findMany({
            skip: skip,
            take: take,
            orderBy: {
              tanggal_post: 'desc',
            },
          });
          if (!activity) {
            const response = {
              execution: false,
              stack: 'getActivity',
              message: {
                message: 'Failed to find data',
                status_code: 500,
              },
              data: {},
            };
            return response;
          }
          const data: object[] = [];
          for (const activity_list of activity) {
            const processImage = await this.findIMGAktivitasORG(
              activity_list.id,
            );
            const Owner = await this.findOwnerProduct(activity_list.yayasan_id);
            const processedData = {
              id: activity_list.id,
              judul_berita: activity_list.judul_berita,
              berita_acara: activity_list.berita_acara,
              lokasi_alamat: activity_list.lokasi_alamat,
              lokasi_kelurahan: activity_list.lokasi_kelurahan,
              lokasi_kecamatan: activity_list.lokasi_kecamatan,
              lokasi_provinsi: activity_list.lokasi_provinsi,
              tanggal_post: activity_list.tanggal_post,
              image: processImage,
              owner: Owner,
            };
            data.push(processedData);
          }
          const response = {
            execution: true,
            stack: 'done',
            message: {
              message: 'Generate sucessfull',
              status_code: 200,
            },
            data: data,
          };
          return response;
        }
      }
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async findOrganizationParam(
    keyword?: string,
    provinsi?: string,
  ): Promise<StandardResponse> {
    try {
      const Organization: any[] = [];
      if (keyword && !provinsi) {
        const yayasanKeyword = await this.prisma.yayasan.findMany({
          where: {
            fullname_organisasi: { contains: keyword },
          },
        });
        if (yayasanKeyword) {
          for (const org of yayasanKeyword) {
            const findIMGOrg = await this.findIMGCoverORG(org.id);
            Organization.push({ ...org, image: findIMGOrg });
          }
        }
      } else if (provinsi && !keyword) {
        const yayasan = await this.prisma.yayasan.findMany({
          where: {
            provinsi: { equals: provinsi },
          },
        });
        if (yayasan) {
          for (const org of yayasan) {
            const findIMGOrg = await this.findIMGCoverORG(org.id);
            Organization.push({ ...org, image: findIMGOrg });
          }
          // console.log(yayasan);
          // Organization.push(...yayasan);
        }
      } else {
        const yayasanByParam = await this.prisma.yayasan.findMany({
          where: {
            OR: [
              {
                provinsi: { equals: provinsi },
                fullname_organisasi: { contains: keyword },
              },
            ],
          },
        });
        if (yayasanByParam) {
          for (const org of yayasanByParam) {
            const findIMGOrg = await this.findIMGCoverORG(org.id);
            Organization.push({ ...org, image: findIMGOrg });
          }
          // console.log(yayasanByParam);
          // Organization.push(...yayasanByParam);
        }
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Generate sucessfull',
          status_code: 200,
        },
        data: Organization,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async find_DetailCharity(
    userData?: any,
    yayasan_id?: string,
  ): Promise<StandardResponse> {
    try {
      let yayasan: object;
      if (!userData && yayasan_id) {
        const findyayasan = await this.prisma.yayasan.findFirst({
          where: { id: yayasan_id },
        });
        if (!findyayasan) {
          const response = {
            execution: false,
            stack: 'Find',
            message: {
              message: 'Cannot find organization',
              status_code: 404,
            },
            data: {},
          };
          return response;
        }
        const findlogo = await this.findIMGLogoORG(findyayasan.id);
        const aktivitasOrg = await this.findAktivitasOrg(findyayasan.id);
        const productOrg = await this.findProductOrg(findyayasan.id);
        const findIMGOrg = await this.findIMGCoverORG(findyayasan.id);
        yayasan = {
          id: findyayasan.id,
          user_id: findyayasan.user_id,
          fullname_organisasi: findyayasan.fullname_organisasi,
          phone: findyayasan.phone,
          tentang_organisasi: findyayasan.tentang_organisasi,
          image: findIMGOrg,
          website: findyayasan.website,
          nomer_izin: findyayasan.nomer_izin,
          status: findyayasan.status_confirmed,
          link_fb: findyayasan.link_fb,
          link_instagram: findyayasan.link_instagram,
          link_twitter: findyayasan.link_twitter,
          tanggal_post: findyayasan.tanggal_post,
          logo_yayasan: findlogo,
          aktivitas: aktivitasOrg,
          product: productOrg,
        };
      } else if (userData && !yayasan_id) {
        const findyayasan = await this.prisma.yayasan.findFirst({
          where: { user_id: userData.user_id },
        });
        if (!findyayasan) {
          const response = {
            execution: false,
            stack: 'Find',
            message: {
              message: 'Cannot find organization',
              status_code: 404,
            },
            data: {},
          };
          return response;
        }
        const findlogo = await this.findIMGLogoORG(findyayasan.id);
        const aktivitasOrg = await this.findAktivitasOrg(findyayasan.id);
        const productOrg = await this.findProductOrg(findyayasan.id);
        const findIMGOrg = await this.findIMGCoverORG(findyayasan.id);
        yayasan = {
          id: findyayasan.id,
          user_id: findyayasan.user_id,
          fullname_organisasi: findyayasan.fullname_organisasi,
          phone: findyayasan.phone,
          no_rek: findyayasan.no_rek,
          atas_nama: findyayasan.atas_nama,
          nama_bank: findyayasan.nama_bank,
          nomor_izin: findyayasan.nomer_izin,
          tentang_organisasi: findyayasan.tentang_organisasi,
          alamat: findyayasan.alamat,
          provinsi: findyayasan.provinsi,
          image: findIMGOrg,
          website: findyayasan.website,
          nomer_izin: findyayasan.nomer_izin,
          status: findyayasan.status_confirmed,
          link_fb: findyayasan.link_fb,
          link_instagram: findyayasan.link_instagram,
          link_twitter: findyayasan.link_twitter,
          tanggal_post: findyayasan.tanggal_post,
          logo_yayasan: findlogo,
          aktivitas: aktivitasOrg,
          product: productOrg,
        };
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Success Find Detail Charity',
          status_code: 200,
        },
        data: yayasan,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }

  async findActivitasParam(
    keyword?: string,
    provinsi?: string,
  ): Promise<StandardResponse> {
    try {
      const Organization: any[] = [];
      if (keyword && !provinsi) {
        const yayasanKeyword = await this.prisma.aktivitas_organisasi.findMany({
          where: {
            judul_berita: { contains: keyword },
          },
        });
        const activity: any[] = [];
        for (const org of yayasanKeyword) {
          const yayasan = await this.findOwnerProduct(org.yayasan_id);
          const cover = await this.findIMGAktivitasORG(org.id);
          const dataRes = {
            id: org.id,
            judul_berita: org.judul_berita,
            berita_acara: org.berita_acara,
            lokasi_alamat: org.lokasi_alamat,
            lokasi_kelurahan: org.lokasi_kelurahan,
            lokasi_kecamatan: org.lokasi_kecamatan,
            lokasi_provinsi: org.lokasi_provinsi,
            tanggal_post: org.tanggal_post,
            image: cover,
            owner: yayasan,
          };
          activity.push(dataRes);
        }
        if (yayasanKeyword) {
          Organization.push(...activity);
        }
      } else if (provinsi && !keyword) {
        const yayasanProvinsi = await this.prisma.aktivitas_organisasi.findMany(
          {
            where: {
              lokasi_provinsi: { equals: provinsi },
            },
          },
        );
        const activity: any[] = [];
        for (const org of yayasanProvinsi) {
          const findActivity = await this.findOwnerProduct(org.yayasan_id);
          const cover = await this.findIMGAktivitasORG(org.id);
          const dataRes = {
            id: org.id,
            judul_berita: org.judul_berita,
            berita_acara: org.berita_acara,
            lokasi_alamat: org.lokasi_alamat,
            lokasi_kelurahan: org.lokasi_kelurahan,
            lokasi_kecamatan: org.lokasi_kecamatan,
            lokasi_provinsi: org.lokasi_provinsi,
            tanggal_post: org.tanggal_post,
            image: cover,
            owner: findActivity,
          };
          activity.push(dataRes);
        }
        if (yayasanProvinsi) {
          Organization.push(...activity);
        }
      } else {
        const yayasanByParam = await this.prisma.aktivitas_organisasi.findMany({
          where: {
            OR: [
              {
                lokasi_provinsi: { equals: provinsi },
                judul_berita: { contains: keyword },
              },
            ],
          },
        });
        const activity: any[] = [];
        if (yayasanByParam) {
          for (const org of yayasanByParam) {
            const findActivity = await this.findOwnerProduct(org.yayasan_id);
            const cover = await this.findIMGAktivitasORG(org.id);
            const dataRes = {
              id: org.id,
              judul_berita: org.judul_berita,
              berita_acara: org.berita_acara,
              lokasi_alamat: org.lokasi_alamat,
              lokasi_kelurahan: org.lokasi_kelurahan,
              lokasi_kecamatan: org.lokasi_kecamatan,
              lokasi_provinsi: org.lokasi_provinsi,
              tanggal_post: org.tanggal_post,
              image: cover,
              owner: findActivity,
            };
            activity.push(dataRes);
          }
          Organization.push(...activity);
        }
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Generate sucessfull',
          status_code: 200,
        },
        data: Organization,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async findDetailAktivitas(id: string): Promise<StandardResponse> {
    try {
      const findAktivitas = await this.prisma.aktivitas_organisasi.findFirst({
        where: { yayasan_id: id },
      });
      if (!findAktivitas) {
        const response = {
          execution: false,
          stack: 'Find',
          message: {
            message: 'Cannot find organization activity',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }
      const findyayasan = await this.prisma.yayasan.findFirst({
        where: { id: findAktivitas.yayasan_id },
      });
      const findgallery = await this.findGalleryAktivitas(findAktivitas.id);
      const aktivitas = {
        id: findAktivitas.id,
        berita_acara: findAktivitas.berita_acara,
        judul_berita: findAktivitas.judul_berita,
        lokasi_alamat: findAktivitas.lokasi_alamat,
        lokasi_kelurahan: findAktivitas.lokasi_kelurahan,
        lokasi_kecamatan: findAktivitas.lokasi_kecamatan,
        lokasi_provinsi: findAktivitas.lokasi_provinsi,
        tanggal_post: findAktivitas.tanggal_post,
        yayasan: findyayasan,
        gallery: findgallery,
      };
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull find organization activity',
          status_code: 200,
        },
        data: aktivitas,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async findGalleryAktivitas(id: string): Promise<object> {
    try {
      const findAktivitasOrg = await this.prisma.gallery_aktivitas.findMany({
        where: { aktivitas_id: id },
      });
      const processedAktivitasGallery = [];

      for (const aktivitas of findAktivitasOrg) {
        if (aktivitas == null) {
          const imageUrl = {};
          const processedData = { url: imageUrl };
          processedAktivitasGallery.push(processedData);
        } else {
          const base = this.config.get<string>('NEST_BASEURL');
          const imageUrl = `${base}uploads/${aktivitas.name_file}`;
          const processedData = { url: imageUrl };
          processedAktivitasGallery.push(processedData);
        }
      }
      return processedAktivitasGallery;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async findDetailOrganization(userData: any) {
    try {
      const checkPermission = await this.prisma.users.findFirst({
        where: { id: { equals: userData.userId } },
      });
      if (!checkPermission) {
        const response = {
          execution: false,
          stack: 'CheckResult',
          message: {
            message: 'User Is Not Found',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }
      const findORG = await this.prisma.yayasan.findMany({
        where: { user_id: { equals: userData.userId } },
      });
      if (findORG.length == 0) {
        const response = {
          execution: false,
          stack: 'Find',
          message: {
            message: 'No Data found',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }
      const processedAktivitas = [];
      for (const org of findORG) {
        const findlogo = await this.findIMGLogoORG(org.id);
        const findIMGOrg = await this.findIMGCoverORG(org.id);
        const yayasan = {
          id: org.id,
          user_id: org.user_id,
          fullname_organisasi: org.fullname_organisasi,
          phone: org.phone,
          tentang_organisasi: org.tentang_organisasi,
          image: findIMGOrg,
          website: org.website,
          nomer_izin: org.nomer_izin,
          status: org.status_confirmed,
          link_fb: org.link_fb,
          link_instagram: org.link_instagram,
          link_twitter: org.link_twitter,
          tanggal_post: org.tanggal_post,
          logo_yayasan: findlogo,
        };
        processedAktivitas.push(yayasan);
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Successfull delete your organization activity',
          status_code: 200,
        },
        data: processedAktivitas,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  // async findDetailOrganization(id: string): Promise<StandardResponse> {
  //   try {
  //     const findOrg = await this.prisma.yayasan.findFirst({
  //       where: { id: id },
  //     });
  //     const response = {
  //       execution: true,
  //       stack: 'done',
  //       message: {
  //         message: 'Sucessfull find organization',
  //         status_code: 200,
  //       },
  //       data: findOrg,
  //     };
  //     return response;
  //   } catch (err) {
  //     this.logger.log({
  //       level: 'error',
  //       message: 'Exeption Handler Looging for',
  //       err: err,
  //       errCustomCode: '20',
  //     });
  //     throw new PrismaClientExceptionFilter();
  //   }
  // }
  async postIMGList(
    file: Express.Multer.File,
    param: Partial<ImgListParams> = {},
  ) {
    try {
      let saveimglist: any;
      const name = file.originalname.split('.')[0];
      const fileExtension = path.extname(file.originalname);

      if (param.aktivitas_id != null) {
        const fileName = `${name}-${param.aktivitas_id}${fileExtension}`;
        saveimglist = await this.prisma.image_list.create({
          data: {
            aktivitas_id: param.aktivitas_id,
            user_id: null,
            yayasan_id: null,
            name_file: fileName,
            command: param.command,
          },
        });
        // Save file to server
        const uploadPath = path.join('./clients/img', fileName);
        await fs.promises.writeFile(uploadPath, file.buffer);
      } else if (param.user_id != null) {
        const newFileName = `${name}-${param.user_id}${fileExtension}`;
        saveimglist = await this.prisma.image_list.create({
          data: {
            aktivitas_id: null,
            user_id: param.user_id,
            yayasan_id: null,
            name_file: newFileName,
            command: param.command,
          },
        });
      } else {
        const fileName = `${name}-${param.yayasan_id}${fileExtension}`;
        saveimglist = await this.prisma.image_list.create({
          data: {
            aktivitas_id: null,
            user_id: null,
            yayasan_id: param.yayasan_id,
            name_file: fileName,
            command: param.command,
          },
        });

        // Save file to server
        const uploadPath = path.join('./clients/img', fileName);
        await fs.promises.writeFile(uploadPath, file.buffer);
      }
      return saveimglist;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async postAktivitas(
    userData: any,
    id: string,
    RegisterAktivitasDto: RegisterAktivitasDto,
    file: Express.Multer.File,
  ): Promise<StandardResponse> {
    try {
      // check user request
      const users = userData;
      const yayasan_id = id;
      const checkValidUser = await this.prisma.yayasan.count({
        where: {
          id: yayasan_id,
          user_id: users.userId,
        },
      });

      if (checkValidUser === 0) {
        const response = {
          execution: false,
          stack: 'ValidUser',
          message: {
            message: 'You`re don`t have access, Invalid Credential User',
            status_code: 403,
          },
          data: {},
        };
        return response;
      }
      // Validate file size
      const fileSizeLimit = 1024 * 1024; // 1MB
      if (file.size > fileSizeLimit) {
        throw new BadRequestException({
          message: 'File size too large. Maximum size allowed is 1MB.',
          statusCode: 400,
        });
      }
      // Validate file type
      const allowedExtensions = ['.jpg', '.jpeg', '.png'];
      const fileExtension = path.extname(file.originalname).toLowerCase();
      if (!allowedExtensions.includes(fileExtension)) {
        throw new BadRequestException({
          message: 'Only image files (jpg, jpeg, png) are allowed.',
          statusCode: 400,
        });
      }
      const myCuid = await this.generateCuid();
      const myDate = new Date();
      const myEpoch = Math.floor(myDate.getTime() / 1000);
      console.error(RegisterAktivitasDto);
      console.error(myEpoch);
      const register = await this.prisma.aktivitas_organisasi.create({
        data: {
          id: myCuid,
          yayasan_id: id,
          berita_acara: RegisterAktivitasDto.berita_acara,
          judul_berita: RegisterAktivitasDto.judul_berita,
          lokasi_alamat: RegisterAktivitasDto.lokasi_alamat,
          lokasi_kelurahan: RegisterAktivitasDto.lokasi_kelurahan,
          lokasi_kecamatan: RegisterAktivitasDto.lokasi_kecamatan,
          lokasi_provinsi: RegisterAktivitasDto.lokasi_provinsi,
          tanggal_post: myEpoch,
        },
      });
      if (!register) {
        const response = {
          execution: false,
          stack: 'Create',
          message: {
            message: 'Cannot Create organization activity',
            status_code: 500,
          },
          data: {},
        };
        return response;
      }
      const ImgListParams = {
        aktivitas_id: myCuid,
        user_id: null,
        yayasan_id: null,
        command: 'Aktivitas Cover',
      };
      const saveimglist = await this.postIMGList(file, ImgListParams);
      if (!saveimglist) {
        const response = {
          execution: false,
          stack: 'imglist',
          message: {
            message: 'Cannot Save Img List',
            status_code: 500,
          },
          data: {},
        };
        return response;
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Successfull create aktivity organization',
          status_code: 201,
        },
        data: {},
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async save_imgGallery(files: Express.Multer.File[], aktivitas_id: string) {
    const allowedExtensions = ['.jpg', '.jpeg', '.png'];
    const maxSize = 1 * 1024 * 1024; // 1MB
    const savedFiles: any[] = [];
    const filePromises = files.map(async (file) => {
      if (
        !allowedExtensions.includes(file.originalname.toLowerCase().slice(-4))
      ) {
        throw new BadRequestException(
          `Invalid file extension for file '${file.originalname}'. Only JPG/JPEG are allowed.`,
        );
      }

      if (file.size > maxSize) {
        throw new BadRequestException(
          `File size exceeds the limit for file '${file.originalname}'. Max size is 1MB.`,
        );
      }
      try {
        const myCuid = cuid();
        const filePath = path.join('./clients/img', file.originalname);
        const savedFile = await this.prisma.gallery_aktivitas.create({
          data: {
            id: myCuid,
            aktivitas_id: aktivitas_id,
            name_file: file.originalname,
            command: 'Gallery Aktivitas',
          },
        });
        // Simpan file ke sistem file
        await fs.promises.writeFile(filePath, file.buffer);
        savedFiles.push(savedFile);
        return savedFile;
      } catch (err) {
        this.logger.log({
          level: 'error',
          message: 'Exeption Handler Looging for',
          err: err,
          errCustomCode: '20',
        });
        throw new PrismaClientExceptionFilter();
        // throw new BadRequestException('Failed to save file to database');
      }
    });
    return Promise.all(filePromises);
  }

  //Put Controller
  async updateActivityORG(
    activity_id: string,
    dto: RegisterAktivitasDto,
    files: any,
  ): Promise<StandardResponse> {
    try {
      const findYayasan = await this.prisma.aktivitas_organisasi.findFirst({
        where: { id: activity_id },
      });
      if (findYayasan == null) {
        const response = {
          execution: false,
          stack: 'imglist',
          message: {
            message: 'Cannot find organization info',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }

      // Lakukan operasi Update Aktivitas
      const updatedOrganization = await this.prisma.aktivitas_organisasi.update(
        {
          where: { id: activity_id },
          data: {
            judul_berita: dto.judul_berita,
            berita_acara: dto.berita_acara,
            lokasi_alamat: dto.lokasi_alamat,
            lokasi_kelurahan: dto.lokasi_kelurahan,
            lokasi_kecamatan: dto.lokasi_kecamatan,
            lokasi_provinsi: dto.lokasi_provinsi,
          },
        },
      );
      if (!updatedOrganization) {
        const response = {
          execution: false,
          stack: 'imglist',
          message: {
            message: 'Cannot update activity organization info',
            status_code: 500,
          },
          data: {},
        };
        return response;
      }
      //Check if cover != null jalankan proses update image cover org
      if (files[0].cover != null) {
        // Validasi file pertama
        if (!this.isValidFile(files[0].cover)) {
          const response = {
            execution: false,
            stack: 'Validation File',
            message: {
              message:
                'your uploaded is not valid. Allowed file types are jpg, jpeg, png with maximum size of 1MB.',
              status_code: 403,
            },
            data: {},
          };
          return response;
        }
        // Dapatkan data lama dari tabel image_list berdasarkan command Organization Cover
        const oldImageDataCover = await this.prisma.image_list.findFirst({
          where: { aktivitas_id: activity_id, command: 'Aktivitas Cover' },
        }); /* Query ke database untuk mendapatkan data lama */
        if (oldImageDataCover === null) {
          const saveNewImage = await this.prisma.image_list.create({
            data: {
              aktivitas_id: activity_id,
              name_file: files[0].cover.originalname,
              command: 'Aktivitas Cover',
            },
          });
          if (!saveNewImage) {
            const response = {
              execution: false,
              stack: 'Save image',
              message: {
                message: 'Failed to save new cover organization',
                status_code: 500,
              },
              data: {},
            };
            return response;
          }
          // Proses file pertama
          const filePath = `./clients/img/${files[0].cover.originalname}`; /* Path untuk menyimpan file pertama */
          fs.writeFileSync(filePath, files[0].cover.buffer);
        } else {
          // Hapus file Organization Cover lama dari direktori ./client/img
          const oldImagePathCover = path.join(
            './clients/img',
            oldImageDataCover.name_file,
          );
          // const oldImagePathCover = path.join(__dirname, `./clients/img/`);
          if (fs.existsSync(oldImagePathCover)) {
            fs.unlinkSync(oldImagePathCover);
          }

          //update image_list cover org
          const updateCover = await this.prisma.image_list.update({
            where: { id: oldImageDataCover.id },
            data: {
              name_file: files[0].cover.originalname,
            },
          });
          if (!updateCover) {
            const response = {
              execution: false,
              stack: 'Save image',
              message: {
                message: 'Failed to save new cover organization',
                status_code: 500,
              },
              data: {},
            };
            return response;
          }

          // Proses file pertama
          const file1Path = `./clients/img/${files[0].cover.originalname}`; /* Path untuk menyimpan file pertama */
          fs.writeFileSync(file1Path, files[0].cover.buffer);
        }
        //return response to controller
        const response = {
          execution: true,
          stack: 'done',
          message: {
            message: 'Successfull update activitas organization',
            status_code: HttpStatus.CREATED,
          },
          data: {},
        };
        return response;
      }
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async updateOrganization(
    userData: any,
    yayasan_id: string,
    dto: UpdateOrganizationDto,
    files: any,
  ): Promise<StandardResponse> {
    try {
      //Proses Pertama Update Organization
      const findYayasan = await this.prisma.yayasan.findFirst({
        where: { id: yayasan_id, user_id: userData.userId },
      });
      if (findYayasan == null) {
        const response = {
          execution: false,
          stack: 'imglist',
          message: {
            message: 'Cannot find organization info',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }
      // Lakukan operasi dengan Prisma untuk update ke database
      const updatedOrganization = await this.prisma.yayasan.update({
        where: { id: yayasan_id, user_id: userData.userId },
        data: {
          fullname_organisasi: dto.fullname_organisasi,
          phone: dto.phone,
          no_rek: dto.norek,
          atas_nama: dto.atas_nama,
          alamat: dto.alamat,
          tentang_organisasi: dto.tentang_organisasi,
          website: dto.website,
          nomer_izin: dto.nomer_izin,
          link_fb: dto.link_fb,
          link_instagram: dto.link_instagram,
          link_twitter: dto.link_twitter,
        },
      });
      if (!updatedOrganization) {
        const response = {
          execution: false,
          stack: 'imglist',
          message: {
            message: 'Cannot update organization info',
            status_code: 500,
          },
          data: {},
        };
        return response;
      }

      //Check if cover != null jalankan proses update image cover org
      if (files[0].cover != null) {
        // Validasi file pertama
        if (!this.isValidFile(files[0].cover)) {
          const response = {
            execution: false,
            stack: 'Validation File',
            message: {
              message:
                'your uploaded is not valid. Allowed file types are jpg, jpeg, png with maximum size of 1MB.',
              status_code: 403,
            },
            data: {},
          };
          return response;
        }

        // Dapatkan data lama dari tabel image_list berdasarkan command Organization Cover
        const oldImageDataCover = await this.prisma.image_list.findFirst({
          where: { yayasan_id: yayasan_id, command: 'Organization Cover' },
        }); /* Query ke database untuk mendapatkan data lama */
        if (oldImageDataCover === null) {
          const saveNewImage = await this.prisma.image_list.create({
            data: {
              yayasan_id: yayasan_id,
              name_file: files[0].cover.originalname,
              command: 'Organization Cover',
            },
          });
          if (!saveNewImage) {
            const response = {
              execution: false,
              stack: 'Save image',
              message: {
                message: 'Failed to save new cover organization',
                status_code: 500,
              },
              data: {},
            };
            return response;
          }
          // Proses file pertama
          const filePath = `./clients/img/${files[0].cover.originalname}`; /* Path untuk menyimpan file pertama */
          fs.writeFileSync(filePath, files[0].cover.buffer);
        } else {
          // Hapus file Organization Cover lama dari direktori ./client/img
          const oldImagePathCover = path.join(
            './clients/img',
            oldImageDataCover.name_file,
          );
          // const oldImagePathCover = path.join(__dirname, `./clients/img/`);
          if (fs.existsSync(oldImagePathCover)) {
            fs.unlinkSync(oldImagePathCover);
          }

          //update image_list cover org
          const updateCover = await this.prisma.image_list.update({
            where: { id: oldImageDataCover.id },
            data: {
              name_file: files[0].cover.originalname,
            },
          });
          if (!updateCover) {
            const response = {
              execution: false,
              stack: 'Save image',
              message: {
                message: 'Failed to save new cover organization',
                status_code: 500,
              },
              data: {},
            };
            return response;
          }

          // Proses file pertama
          const file1Path = `./clients/img/${files[0].cover.originalname}`; /* Path untuk menyimpan file pertama */
          fs.writeFileSync(file1Path, files[0].cover.buffer);
        }
      }

      //

      //Check if logo != null jalankan proses update image logo org
      if (files[1].logo !== null) {
        // Validasi file kedua
        if (!this.isValidFile(files[1].logo)) {
          const response = {
            execution: false,
            stack: 'Validation File',
            message: {
              message:
                'your uploaded is not valid. Allowed file types are jpg, jpeg, png with maximum size of 1MB.',
              status_code: 403,
            },
            data: {},
          };
          return response;
        }

        // Dapatkan data lama dari tabel image_list berdasarkan command Organization Logo
        const oldImageDataLogo = await this.prisma.image_list.findFirst({
          where: { yayasan_id: yayasan_id, command: 'Organization Logo' },
        }); /* Query ke database untuk mendapatkan data lama */
        if (oldImageDataLogo === null) {
          const saveNewImage = await this.prisma.image_list.create({
            data: {
              yayasan_id: yayasan_id,
              name_file: files[1].logo.originalname,
              command: 'Organization Logo',
            },
          });
          if (!saveNewImage) {
            const response = {
              execution: false,
              stack: 'Save image',
              message: {
                message: 'Failed to save new logo organization',
                status_code: 500,
              },
              data: {},
            };
            return response;
          }
          // Proses file pertama
          const filePath = `./clients/img/${files[0].logo.originalname}`; /* Path untuk menyimpan file pertama */
          fs.writeFileSync(filePath, files[1].logo.buffer);
        } else {
          // Hapus file Organization Logo lama dari direktori ./client/img
          const oldImagePathLogo = path.join(
            './clients/img',
            oldImageDataLogo.name_file,
          );
          if (fs.existsSync(oldImagePathLogo)) {
            fs.unlinkSync(oldImagePathLogo);
          }

          //Update ImageList Cover Organization
          const updateImageList = await this.prisma.image_list.update({
            where: { id: oldImageDataLogo.id },
            data: {
              name_file: files[1].logo.originalname,
            },
          });

          if (!updateImageList) {
            const response = {
              execution: false,
              stack: 'Save image',
              message: {
                message: 'Failed to save new cover organization',
                status_code: 500,
              },
              data: {},
            };
            return response;
          }

          // Proses file kedua
          const file2Path = `./clients/img/${files[1].logo.originalname}`; /* Path untuk menyimpan file kedua */
          fs.writeFileSync(file2Path, files[1].logo.buffer);
        }
      }

      //return response to controller
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Successfull update organization',
          status_code: HttpStatus.CREATED,
        },
        data: {},
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }

  private isValidFile(file: Express.Multer.File): boolean {
    if (!file) {
      return false;
    }

    const allowedExtensions = ['.jpg', '.jpeg', '.png'];
    const maxSizeInBytes = 1024 * 1024; // 1MB

    const fileExtension = path.extname(file.originalname).toLowerCase();
    if (!allowedExtensions.includes(fileExtension)) {
      return false;
    }

    if (file.size > maxSizeInBytes) {
      return false;
    }

    return true;
  }

  //Delete Services
  async deleteAktivitas(aktivitas_id: string) {
    try {
      //find data gallery aktivitas
      const findGallery = await this.prisma.gallery_aktivitas.findMany({
        where: { aktivitas_id: aktivitas_id },
      });

      if (findGallery.length > 0) {
        for (const gallery of findGallery) {
          const oldImagePathLogo = path.join(
            './clients/img',
            gallery.name_file,
          );
          if (fs.existsSync(oldImagePathLogo)) {
            fs.unlinkSync(oldImagePathLogo);
          }
          const deleteGallery = await this.prisma.gallery_aktivitas.delete({
            where: { id: gallery.id },
          });
          if (deleteGallery) {
            this.logger.log({
              level: 'info',
              message: `Information Activity for Delete Gallery Aktivitas With Id: ${gallery.id}`,
              err: {},
              errCustomCode: '20',
            });
          }
        }
      }
      const fintAktivitas = await this.prisma.aktivitas_organisasi.delete({
        where: { id: aktivitas_id },
      });
      if (fintAktivitas) {
        this.logger.log({
          level: 'info',
          message: `Information Activity for Delete Aktivitas With Id: ${aktivitas_id}`,
          err: {},
          errCustomCode: '20',
        });
      } else {
        const response = {
          execution: false,
          stack: 'Delete Aktivitas',
          message: {
            message: 'Failed to deleting your aktivity',
            status_code: 500,
          },
          data: {},
        };
        return response;
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Successfull delete your organization activity',
          status_code: 204,
        },
        data: {},
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }

  async deleteGallery(id: string) {
    try {
      const findGallery = await this.prisma.gallery_aktivitas.findFirst({
        where: { id: id },
      });
      if (findGallery == null) {
        const response = {
          execution: false,
          stack: 'findImage',
          message: {
            message: 'Data Is Not Found',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }
      return await this.prisma.gallery_aktivitas.delete({
        where: { id: id },
      });
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  // Prosess for private mode
  private async generateCuid(): Promise<string> {
    const Mycuid = cuid();
    return Mycuid;
  }
  private async findProductOrg(yayasan_id: string) {
    try {
      const findProductOrg = await this.prisma.product_organisasi.findMany({
        where: { yayasan_id: yayasan_id },
      });
      const processedAktivitas = [];
      const image = [];
      const base = this.config.get<string>('NEST_BASEURL');
      let Url: object;
      for (const product of findProductOrg) {
        const owner = await this.findOwnerProduct(yayasan_id);
        const findProductImage = await this.prisma.product_gallery.findMany({
          where: { product_id: { equals: product.id } },
        });
        for (let index = 0; index < findProductImage.length; index++) {
          if (findProductImage[index] == null) {
            Url = {};
          } else {
            Url = {
              id: findProductImage[index].id,
              link: `${base}img/${findProductImage[index].name_file}`,
            };
          }
          image.push(Url);
        }
        const prosesRating = await this.findRateProduct(product.id);
        const processedData = {
          ...product,
          owner,
          image,
          prosesRating,
        };
        processedAktivitas.push(processedData);
      }
      return processedAktivitas;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async findRateProduct(product_id: string) {
    try {
      const getRate = await this.prisma.product_rating.findMany({
        where: { product_id: product_id },
      });
      const data: object[] = [];
      for (const product of getRate) {
        const processImage = await this.finImageRate(product.id);
        const processedData = {
          id: product.id,
          product_id: product.product_id,
          product_rating: product.product_rating,
          comment: product.comment,
          like: product.like,
          dislike: product.dislike,
          image: processImage,
        };
        data.push(processedData);
      }
      return data;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async finImageRate(rate_id: string) {
    try {
      const base = this.config.get<string>('NEST_BASEURL');
      const getImage = await this.prisma.rating_image.findMany({
        where: {
          AND: [
            { rating_id: rate_id },
            { command: { contains: 'Rating Image for' } },
          ],
        },
      });
      const processImage: object[] = [];
      let Url: object;
      for (const Image of getImage) {
        if (Image == null) {
          Url = {};
        } else {
          Url = {
            id: Image.id,
            name_file: Image.name_file,
            link: `${base}img/${Image.name_file}`,
          };
        }
        processImage.push(Url);
      }
      return processImage;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  //proses array file to db
  private async findOwnerProduct(yayasan_id: string) {
    try {
      const findOrg = await this.prisma.yayasan.findFirst({
        where: { id: yayasan_id },
      });
      let Owner: object;
      let Url: object;
      if (findOrg) {
        const findtPP = await this.prisma.image_list.findFirst({
          where: {
            AND: [
              { yayasan_id: yayasan_id },
              { command: { contains: 'Organization Logo' } },
            ],
          },
        });
        const base = this.config.get<string>('NEST_BASEURL');
        if (findtPP == null) {
          Url = { url: null };
        } else {
          Url = {
            url: `${base}img/${findtPP.name_file}`,
          };
        }
        Owner = {
          fullname_organisasi: findOrg.fullname_organisasi,
          provinsi: findOrg.provinsi,
          logo: Url,
        };
        return Owner;
      } else {
        Owner = {};
        return Owner;
      }
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async findAktivitasOrg(id: string): Promise<object> {
    try {
      const findAktivitasOrg = await this.prisma.aktivitas_organisasi.findMany({
        where: { yayasan_id: id },
      });
      const processedAktivitas = [];

      for (const aktivitas of findAktivitasOrg) {
        const findIMGAktivitasORG = await this.findIMGAktivitasORG(
          aktivitas.id,
        );
        const findOwner = await this.findOwnerProduct(id);
        const processedData = {
          ...aktivitas,
          image: findIMGAktivitasORG,
          owner: findOwner,
        };
        processedAktivitas.push(processedData);
      }
      return processedAktivitas;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async findIMGCoverORG(id: string): Promise<object> {
    try {
      const find = await this.prisma.image_list.findFirst({
        where: { yayasan_id: id, command: 'Organization Cover' },
      });
      if (find == null) {
        const imageUrl = { cover: { url: null } };
        return imageUrl;
      }
      const base = this.config.get<string>('NEST_BASEURL');
      const imageUrl = { cover: { url: `${base}img/${find.name_file}` } };
      ``;
      return imageUrl;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async findIMGLogoORG(id: string): Promise<object> {
    try {
      const findlogoORG = await this.prisma.image_list.findFirst({
        where: { yayasan_id: id, command: 'LogoORG' },
      });
      if (findlogoORG == null) {
        const imageUrl = { url: null };
        return imageUrl;
      }
      const base = this.config.get<string>('NEST_BASEURL');
      const imageUrl = { url: `${base}img/${findlogoORG.name_file}` };
      return imageUrl;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async findIMGAktivitasORG(id: string): Promise<object> {
    try {
      const findAktivitasORG = await this.prisma.image_list.findFirst({
        where: {
          AND: [
            { aktivitas_id: id },
            { command: { contains: 'Aktivitas Cover' } },
          ],
        },
      });
      const base = this.config.get<string>('NEST_BASEURL');
      let Url: object;
      if (findAktivitasORG == null) {
        Url = { url: null };
      } else {
        Url = {
          url: `${base}img/${findAktivitasORG.name_file}`,
        };
      }
      return Url;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async save_berkas(cuid: string, file: Express.Multer.File) {
    try {
      const name = file.originalname.split('.')[0];
      const fileExtension = path.extname(file.originalname);
      // Generate unique file name
      const fileName = `${name}-${cuid}${fileExtension}`;
      const save_berkas = await this.prisma.berkas_list.create({
        data: {
          id: cuid,
          yayasan_id: cuid,
          name_file: fileName,
          command: 'Organisation Document Operasional',
        },
      });
      // Save file to server
      const uploadPath = path.join('./clients/files', fileName);
      await fs.promises.writeFile(uploadPath, file.buffer);
      return save_berkas;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
}
