import {
  Controller,
  Post,
  Version,
  UseGuards,
  UploadedFile,
  UseInterceptors,
  Req,
  BadRequestException,
  Body,
  Get,
  Param,
  UploadedFiles,
  Put,
  Delete,
  HttpCode,
  HttpException,
  Query,
} from '@nestjs/common';
import { OrganizationService } from './organization.service';
// import { AuthGuardsMiddleware } from '../../middleware/AuthGuards.middleware';
import {
  FileInterceptor,
  FilesInterceptor,
  FileFieldsInterceptor,
} from '@nestjs/platform-express';
import {
  RegisterOrganizationDto,
  QueryParamAktivitasALL,
  RegisterAktivitasDto,
  UpdateOrganizationDto,
  QueryParamOrganizationALL,
} from './dto';
import { Request } from 'express';
import { AuthGuard } from 'src/common/guards/auth.guard';
import { PublicGuard } from 'src/common/guards/public.guard';

@Controller('organization')
export class OrganizationController {
  constructor(private readonly organizationService: OrganizationService) {}

  @Version('1')
  @Get('refresh/token')
  @UseGuards(AuthGuard)
  async refreshToken() {
    const getProduct = await this.organizationService.getTotalItem();
    if (getProduct.execution === true && getProduct.stack === 'done') {
      return getProduct;
    } else {
      throw new HttpException(
        getProduct.message.message,
        getProduct.message.status_code,
      );
    }
  }

  @Version('1')
  @Post('register')
  @UseGuards(AuthGuard)
  @UseInterceptors(
    FileInterceptor('file', {
      // Konfigurasi untuk validasi ukuran dan tipe file
      fileFilter: (req, file, callback) => {
        const allowedMimeTypes = ['application/pdf'];
        if (!allowedMimeTypes.includes(file.mimetype)) {
          return callback(
            new BadRequestException(
              'Invalid file format. Only PDF is allowed.',
            ),
            false,
          );
        }

        const maxSize = 1 * 1024 * 1024; // 5MB
        if (file.size > maxSize) {
          return callback(
            new BadRequestException(
              'File size exceeds the limit. Max size is 5MB.',
            ),
            false,
          );
        }

        callback(null, true);
      },
    }),
  )
  async registOrganisasi(
    @Req() req: Request,
    @UploadedFile() file: Express.Multer.File,
    @Body() RegistOrganizationDto: RegisterOrganizationDto,
  ) {
    const userData = req['user'];
    const registOrg = await this.organizationService.registerOrg(
      RegistOrganizationDto,
      userData,
      file,
    );
    if (registOrg.execution === true && registOrg.stack === 'done') {
      // throw new HttpException(registOrg.message, registOrg.message.status_code);
      return registOrg;
    } else {
      throw new HttpException(
        registOrg.message.message,
        registOrg.message.status_code,
      );
    }
  }
  @Version('1')
  @Post('aktivitas/post/:yayasan_id')
  @UseGuards(AuthGuard)
  @UseInterceptors(
    FileInterceptor('image', {
      // Konfigurasi untuk validasi ukuran dan tipe file
      fileFilter: (req, file, callback) => {
        const allowedMimeTypes = ['image/jpeg', 'image/jpg', 'image/png'];
        if (!allowedMimeTypes.includes(file.mimetype)) {
          return callback(
            new BadRequestException(
              'Invalid file format. Only JPEG, JPG, and PNG are allowed.',
            ),
            false,
          );
        }

        const maxSize = 1 * 1024 * 1024; // 5MB
        if (file.size > maxSize) {
          return callback(
            new BadRequestException(
              'File size exceeds the limit. Max size is 5MB.',
            ),
            false,
          );
        }

        callback(null, true);
      },
    }),
  )
  async postAktivitas(
    @Req() req: Request,
    @Param('yayasan_id') id: string,
    @UploadedFile() image: Express.Multer.File,
    @Body() registerAktivitasDto: RegisterAktivitasDto,
  ) {
    if (!image) {
      throw new HttpException('No file uploaded', 400);
    }
    const userData = req['user'];
    const registAktivitasOrg = await this.organizationService.postAktivitas(
      userData,
      id,
      registerAktivitasDto,
      image,
    );
    if (
      registAktivitasOrg.execution === true &&
      registAktivitasOrg.stack === 'done'
    ) {
      // throw new HttpException(registOrg.message, registOrg.message.status_code);
      return registAktivitasOrg;
    } else {
      throw new HttpException(
        registAktivitasOrg.message.message,
        registAktivitasOrg.message.status_code,
      );
    }
  }

  @Version('1')
  @Post('/aktivitas/gallery/post/:aktivitas_id')
  @UseGuards(AuthGuard)
  @UseInterceptors(FilesInterceptor('files'))
  async postGalleryAktivitas(
    @UploadedFiles() files: Array<Express.Multer.File>,
    @Param('aktivitas_id') aktivitas_id: string,
  ) {
    if (!files || files.length === 0) {
      throw new BadRequestException('No files were uploaded');
    }
    try {
      const savedFiles = await this.organizationService.save_imgGallery(
        files,
        aktivitas_id,
      );

      return {
        message: 'Files uploaded and saved to database successfully',
        files: savedFiles,
      };
    } catch (error) {
      throw new BadRequestException('Failed to upload files: ' + error.message);
    }
  }

  //GET Controller
  // GET Organization
  @Version('1')
  @Get('/catalog/totalitem')
  @UseGuards(PublicGuard)
  async countTotalItem() {
    const getProduct = await this.organizationService.getTotalItem();
    if (getProduct.execution === true && getProduct.stack === 'done') {
      return getProduct;
    } else {
      throw new HttpException(
        getProduct.message.message,
        getProduct.message.status_code,
      );
    }
  }
  @Version('1')
  @Get()
  @UseGuards(PublicGuard)
  async getAllOrganization(@Query() queryParam: QueryParamOrganizationALL) {
    const page = parseInt(queryParam.page, 10);
    const take = parseInt(queryParam.take, 10);
    let order: boolean;
    if (
      queryParam.order === 'True' ||
      queryParam.order === 'true' ||
      queryParam.order === 'TRUE'
    ) {
      order = true;
    } else {
      order = false;
    }
    let skip: number;
    if (page > 1) {
      skip = (page - 1) * take;
    } else {
      skip = 0;
    }
    const get_all = await this.organizationService.findOrganizationAll(
      skip,
      take,
      order,
    );
    if (get_all.execution === true && get_all.stack === 'done') {
      return get_all;
    } else {
      throw new HttpException(
        get_all.message.message,
        get_all.message.status_code,
      );
    }
  }
  @Version('1')
  @Get('byParam')
  @UseGuards(PublicGuard)
  async getOrganizationByParam(
    @Query() queryParam: { keyword?: string; provinsi?: string },
  ) {
    const keyword = queryParam.keyword;
    const provinsi = queryParam.provinsi;
    const getAll = await this.organizationService.findOrganizationParam(
      keyword,
      provinsi,
    );
    if (getAll.execution === true && getAll.stack === 'done') {
      return getAll;
    } else {
      throw new HttpException(
        getAll.message.message,
        getAll.message.status_code,
      );
    }
  }
  @Version('1')
  @Get('detailcharity')
  @UseGuards(PublicGuard)
  async getDetailCharity(@Query() queryParam: { yayasan_id: string }) {
    const getOne = await this.organizationService.find_DetailCharity(
      queryParam.yayasan_id,
    );
    if (getOne.execution === true && getOne.stack === 'done') {
      return getOne;
    } else {
      throw new HttpException(
        getOne.message.message,
        getOne.message.status_code,
      );
    }
    // return getOne;
  }
  @Version('1')
  @Get('user/charity')
  @UseGuards(AuthGuard)
  async getProtectedCharity(@Req() req: Request) {
    const userData = req['user'];
    const getOne = await this.organizationService.find_DetailCharity(userData);
    if (getOne.execution === true && getOne.stack === 'done') {
      return getOne;
    } else {
      throw new HttpException(
        getOne.message.message,
        getOne.message.status_code,
      );
    }
  }
  @Version('1')
  @Get('user/valid')
  @UseGuards(AuthGuard)
  async getdetailorganization(@Req() req: Request) {
    const userData = req['user'];
    const getOne =
      await this.organizationService.findDetailOrganization(userData);
    if (getOne.execution === true && getOne.stack === 'done') {
      return getOne;
    } else {
      throw new HttpException(
        getOne.message.message,
        getOne.message.status_code,
      );
    }
  }
  // GET Activity
  @Version('1')
  @Get('/activity/catalog/totalitem')
  @UseGuards(PublicGuard)
  async countTotalActivity() {
    const getProduct = await this.organizationService.getTotalActivity();
    if (getProduct.execution === true && getProduct.stack === 'done') {
      return getProduct;
    } else {
      throw new HttpException(
        getProduct.message.message,
        getProduct.message.status_code,
      );
    }
  }
  @Version('1')
  @Get('detailaktivitas/:aktivitas_id')
  @UseGuards(AuthGuard)
  async detail_aktivitas(@Param('aktivitas_id') id: string) {
    const getDetailActivity =
      await this.organizationService.findDetailAktivitas(id);

    if (
      getDetailActivity.execution === true &&
      getDetailActivity.stack === 'done'
    ) {
      return getDetailActivity;
    } else {
      throw new HttpException(
        getDetailActivity.message.message,
        getDetailActivity.message.status_code,
      );
    }
  }
  @Version('1')
  @Get('user/activity/:yayasan_id')
  @UseGuards(AuthGuard)
  async getAllUserActivity(
    @Param('yayasan_id') yayasan_id: string,
    @Query() queryParam: QueryParamAktivitasALL,
  ) {
    const id = yayasan_id;
    const page = parseInt(queryParam.page, 10);
    const take = parseInt(queryParam.take, 10);
    let order: boolean;
    if (
      queryParam.order === 'True' ||
      queryParam.order === 'true' ||
      queryParam.order === 'TRUE'
    ) {
      order = true;
    } else {
      order = false;
    }
    let skip: number;
    if (page > 1) {
      skip = (page - 1) * take;
    } else {
      skip = 0;
    }
    const get_all = await this.organizationService.findActivityAll(
      skip,
      take,
      order,
      id,
    );
    if (get_all.execution === true && get_all.stack === 'done') {
      return get_all;
    } else {
      throw new HttpException(
        get_all.message.message,
        get_all.message.status_code,
      );
    }
  }
  @Version('1')
  @Get('activity')
  @UseGuards(PublicGuard)
  async getAllActivity(@Query() queryParam: QueryParamAktivitasALL) {
    const page = parseInt(queryParam.page, 10);
    const take = parseInt(queryParam.take, 10);
    let order: boolean;
    if (
      queryParam.order === 'True' ||
      queryParam.order === 'true' ||
      queryParam.order === 'TRUE'
    ) {
      order = true;
    } else {
      order = false;
    }
    let skip: number;
    if (page > 1) {
      skip = (page - 1) * take;
    } else {
      skip = 0;
    }
    const get_all = await this.organizationService.findActivityAll(
      skip,
      take,
      order,
    );
    if (get_all.execution === true && get_all.stack === 'done') {
      return get_all;
    } else {
      throw new HttpException(
        get_all.message.message,
        get_all.message.status_code,
      );
    }
  }
  @Version('1')
  @Get('activity/byParam')
  @UseGuards(PublicGuard)
  async getActivityByParam(
    @Query() queryParam: { keyword?: string; provinsi?: string },
  ) {
    const keyword = queryParam.keyword;
    const provinsi = queryParam.provinsi;
    const getAll = await this.organizationService.findActivitasParam(
      keyword,
      provinsi,
    );
    if (getAll.execution === true && getAll.stack === 'done') {
      return getAll;
    } else {
      throw new HttpException(
        getAll.message.message,
        getAll.message.status_code,
      );
    }
  }
  // End Off GET Controller
  // PUT Controller
  @Version('1')
  @Put('activity/put/:activity_id')
  @UseGuards(AuthGuard)
  @UseInterceptors(FileFieldsInterceptor([{ name: 'cover', maxCount: 1 }]))
  async updateActivityOrg(
    @Req() req: Request,
    @Param('activity_id') activity_id: string,
    @UploadedFiles()
    files: { cover: Express.Multer.File[] },
    @Body() dto: RegisterAktivitasDto,
  ) {
    // const userData = req['user'];
    const paramFile = [];
    if (files.cover && files.cover !== null) {
      paramFile.push({ cover: files.cover[0] });
    } else {
      paramFile.push({ cover: null });
    }
    const result = await this.organizationService.updateActivityORG(
      activity_id,
      dto,
      paramFile,
    );
    if (result.execution === true && result.stack === 'done') {
      return result;
    } else {
      throw new HttpException(
        result.message.message,
        result.message.status_code,
      );
      // throw new ForbiddenException(result.message);
    }
  }
  @Version('1')
  @Put('put/:yayasan_id')
  @UseGuards(AuthGuard)
  @UseInterceptors(
    FileFieldsInterceptor([
      { name: 'cover', maxCount: 1 },
      { name: 'logo', maxCount: 1 },
    ]),
  )
  async updateOrganization(
    @Req() req: Request,
    @Param('yayasan_id') yayasan_id: string,
    @UploadedFiles()
    files: { cover: Express.Multer.File[]; logo: Express.Multer.File[] },
    @Body() dto: UpdateOrganizationDto,
  ) {
    const userData = req['user'];
    const paramFile = [];
    if (files.cover && files.cover !== null) {
      paramFile.push({ cover: files.cover[0] });
    } else {
      paramFile.push({ cover: null });
    }

    if (files.logo && files.logo !== null) {
      paramFile.push({ logo: files.logo[0] });
    } else {
      paramFile.push({ logo: null });
    }
    const result = await this.organizationService.updateOrganization(
      userData,
      yayasan_id,
      dto,
      paramFile,
    );
    if (result.execution === true && result.stack === 'done') {
      return result;
    } else {
      throw new HttpException(
        result.message.message,
        result.message.status_code,
      );
      // throw new ForbiddenException(result.message);
    }
  }

  //DELETE Controller
  @Version('1')
  @Delete('/aktivitas/delete/:aktivitas_id')
  @UseGuards(AuthGuard)
  @HttpCode(204)
  async deleteAktivitas(@Param('aktivitas_id') aktivitas_id: string) {
    const deleteAktivitas =
      await this.organizationService.deleteAktivitas(aktivitas_id);
    if (
      deleteAktivitas.execution === true &&
      deleteAktivitas.stack === 'done'
    ) {
      return deleteAktivitas;
    } else {
      throw new HttpException(
        deleteAktivitas.message.message,
        deleteAktivitas.message.status_code,
      );
    }
  }

  @Version('1')
  @Delete('/aktivitas/gallery/delete/:gallery_id')
  @UseGuards(AuthGuard)
  @HttpCode(204)
  async deleteAktivitasGallery(@Param('gallery_id') id: string) {
    return await this.organizationService.deleteGallery(id);
  }
}
