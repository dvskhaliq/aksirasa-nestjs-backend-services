import { IsNotEmpty, IsString } from 'class-validator';

export class ProvinsiServiceDto {
  @IsString()
  @IsNotEmpty()
  provinsi: string;
}
