import { Test, TestingModule } from '@nestjs/testing';
import { CommonServicesController } from './common-services.controller';
import { CommonServicesService } from './common-services.service';

describe('CommonServicesController', () => {
  let controller: CommonServicesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CommonServicesController],
      providers: [CommonServicesService],
    }).compile();

    controller = module.get<CommonServicesController>(CommonServicesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
