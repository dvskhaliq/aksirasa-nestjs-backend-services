import { Injectable, Logger } from '@nestjs/common';
// import { } from './dto/provinsi-service.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { ConfigService } from '@nestjs/config';
import { StandardResponse } from 'src/types';
import { PrismaClientExceptionFilter } from 'src/common/prisma-client-exception/prisma-client-exception.filter';
import cuid = require('cuid');
import * as fs from 'fs';
import path = require('path');

@Injectable()
export class CommonServicesService {
  private logger = new Logger(CommonServicesService.name);
  constructor(
    private prisma: PrismaService,
    private config: ConfigService,
  ) {}

  async getAllCategory(): Promise<StandardResponse> {
    try {
      const getprovinsi = await this.prisma.product_category.findMany();
      if (!getprovinsi) {
        const response = {
          execution: false,
          stack: 'getprovinsi',
          message: {
            message: 'No data found',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Success Find Data Provinsi',
          status_code: 200,
        },
        data: getprovinsi,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async getHomeCover(): Promise<StandardResponse> {
    try {
      const getImage = await this.prisma.image_list.findMany({
        where: {
          command: { equals: 'Home Cover' },
        },
      });
      if (!getImage) {
        const response = {
          execution: false,
          stack: 'getImageCover',
          message: {
            message: 'No data found',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }
      const Res = [];
      let Url: string;
      const base = this.config.get<string>('NEST_BASEURL');
      for (let index = 0; index < getImage.length; index++) {
        if (getImage == null) {
          Url = null;
        } else {
          Url = `${base}img/${getImage[index].name_file}`;
        }
        Res.push({ url: Url });
      }
      //return response to controller
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Successfull update organization',
          status_code: 200,
        },
        data: Res,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async postHomeCover(userData: any, file: Express.Multer.File) {
    try {
      const checkPermission = await this.prisma.users.findFirst({
        where: { id: { equals: userData.userId } },
      });
      if (!checkPermission) {
        const response = {
          execution: false,
          stack: 'CheckResult',
          message: {
            message: 'User Is Not Found',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }
      if (checkPermission.role !== 'Admin') {
        const response = {
          execution: false,
          stack: 'CheckPermission',
          message: {
            message: `You Don't Have Access Permission`,
            status_code: 400,
          },
          data: {},
        };
        return response;
      }
      const myCuid = cuid();
      const saveImage = await this.save_image(myCuid, file);
      if (!saveImage) {
        const response = {
          execution: false,
          stack: 'Save Berkas',
          message: {
            message: 'Failed to upload your file',
            status_code: 400,
          },
          data: {},
        };
        return response;
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Success Post Home Cover',
          status_code: 201,
        },
        data: {},
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async save_image(cuid: string, file: Express.Multer.File) {
    try {
      const name = file.originalname.split('.')[0];
      const fileExtension = path.extname(file.originalname);
      // Generate unique file name
      const fileName = `${name}-${cuid}${fileExtension}`;
      const save_berkas = await this.prisma.image_list.create({
        data: {
          id: cuid,
          name_file: fileName,
          command: 'Home Cover',
        },
      });
      // Save file to server
      const uploadPath = path.join('./clients/img', fileName);
      await fs.promises.writeFile(uploadPath, file.buffer);
      return save_berkas;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  // async postProvinsi(provinsiDto: ProvinsiServiceDto) {
  //   try {
  //     const postProvinsi = await this.prisma.provinsi.create({
  //       data: {
  //         provinsi_name: provinsiDto.provinsi,
  //       },
  //     });
  //     if (!postProvinsi) {
  //       const response = {
  //         execution: false,
  //         stack: 'post',
  //         message: {
  //           message: 'Failed to post rating',
  //           status_code: 500,
  //         },
  //         data: {},
  //       };
  //       return response;
  //     }
  //     const response = {
  //       execution: true,
  //       stack: 'done',
  //       message: {
  //         message: 'Sucessfull create provinsi',
  //         status_code: 201,
  //       },
  //       data: {},
  //     };
  //     return response;
  //   } catch (err) {
  //     this.logger.log({
  //       level: 'error',
  //       message: 'Exeption Handler Looging for',
  //       err: err,
  //       errCustomCode: '20',
  //     });
  //     throw new PrismaClientExceptionFilter();
  //   }
  // }
}
