import { Module } from '@nestjs/common';
import { CommonServicesService } from './common-services.service';
import { CommonServicesController } from './common-services.controller';
import { JwtModule } from '@nestjs/jwt';
import { RajaongkirServices } from 'src/http/rajaongkir-services/rajaongkir.service';
import { PrismaModule } from 'src/prisma/prisma.module';
import { AuthService } from '../auth/auth.service';

@Module({
  imports: [PrismaModule, JwtModule],
  controllers: [CommonServicesController],
  providers: [CommonServicesService, RajaongkirServices, AuthService],
})
export class CommonServicesModule {}
