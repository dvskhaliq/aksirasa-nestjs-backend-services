import { Test, TestingModule } from '@nestjs/testing';
import { CommonServicesService } from './common-services.service';

describe('CommonServicesService', () => {
  let service: CommonServicesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CommonServicesService],
    }).compile();

    service = module.get<CommonServicesService>(CommonServicesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
