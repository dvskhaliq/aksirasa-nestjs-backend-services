import {
  Controller,
  Get,
  Post,
  // Body,
  // Patch,
  // Param,
  // Delete,
  Version,
  UseGuards,
  HttpException,
  UseInterceptors,
  BadRequestException,
  Req,
  UploadedFile,
} from '@nestjs/common';
import { CommonServicesService } from './common-services.service';
import { RajaongkirServices } from '../../http/rajaongkir-services/rajaongkir.service';
import { AuthGuard } from 'src/common/guards/auth.guard';
import { PublicGuard } from 'src/common/guards/public.guard';
import {
  FileInterceptor,
  // FilesInterceptor,
  // FileFieldsInterceptor,
} from '@nestjs/platform-express';
import { Request } from 'express';
// import { AuthGuardsMiddleware } from 'src/middleware/AuthGuards.middleware';

@Controller('common-services')
export class CommonServicesController {
  constructor(
    private readonly commonServicesService: CommonServicesService,
    private readonly rajaongkirService: RajaongkirServices,
  ) {}
  @Version('1')
  @UseGuards(PublicGuard)
  @Get('/provinsi/get')
  public async getAllProvinsi() {
    const getAllProvinsi = await this.rajaongkirService.getAllProvinsi();
    if (getAllProvinsi.execution === true && getAllProvinsi.stack === 'done') {
      return getAllProvinsi;
    } else {
      throw new HttpException(
        getAllProvinsi.message.message,
        getAllProvinsi.message.status_code,
      );
    }
  }
  @Version('1')
  @UseGuards(PublicGuard)
  @Get('/category/get')
  public async getAllCategory() {
    const getAllProvinsi = await this.commonServicesService.getAllCategory();
    if (getAllProvinsi.execution === true && getAllProvinsi.stack === 'done') {
      return getAllProvinsi;
    } else {
      throw new HttpException(
        getAllProvinsi.message.message,
        getAllProvinsi.message.status_code,
      );
    }
  }
  @Version('1')
  @UseGuards(PublicGuard)
  @Get('/image/cover')
  async getCoverHome() {
    const getCover = await this.commonServicesService.getHomeCover();
    if (getCover.execution === true && getCover.stack === 'done') {
      return getCover;
    } else {
      throw new HttpException(
        getCover.message.message,
        getCover.message.status_code,
      );
    }
  }
  @Version('1')
  @UseGuards(AuthGuard)
  @Post('/image/cover')
  @UseInterceptors(
    FileInterceptor('image', {
      // Konfigurasi untuk validasi ukuran dan tipe file
      fileFilter: (req, file, callback) => {
        const allowedMimeTypes = ['image/jpeg', 'image/jpg', 'image/png'];
        if (!allowedMimeTypes.includes(file.mimetype)) {
          return callback(
            new BadRequestException(
              'Invalid file format. Only jpg, jpeg, png is allowed.',
            ),
            false,
          );
        }

        const maxSize = 1 * 1024 * 1024; // 1MB
        if (file.size > maxSize) {
          return callback(
            new BadRequestException(
              'File size exceeds the limit. Max size is 1MB.',
            ),
            false,
          );
        }

        callback(null, true);
      },
    }),
  )
  async postCoverHome(
    @Req() req: Request,
    @UploadedFile() image: Express.Multer.File,
  ) {
    const userData = req['user'];
    const getCover = await this.commonServicesService.postHomeCover(
      userData,
      image,
    );
    if (getCover.execution === true && getCover.stack === 'done') {
      return getCover;
    } else {
      throw new HttpException(
        getCover.message.message,
        getCover.message.status_code,
      );
    }
  }
}
