import { Injectable, Logger } from '@nestjs/common';
import { SignUpDto, SignInDto } from './dto';
import { PrismaService } from '../../prisma/prisma.service';
import * as argon from 'argon2';
import { PrismaClientExceptionFilter } from 'src/common/prisma-client-exception/prisma-client-exception.filter';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { StandardResponse } from 'src/types';
import cuid = require('cuid');

@Injectable()
export class AuthService {
  private logger = new Logger(AuthService.name);
  constructor(
    private prisma: PrismaService,
    private jwt: JwtService,
    private config: ConfigService,
  ) {}
  async signup(dto: SignUpDto): Promise<StandardResponse> {
    // generate password
    try {
      const hash = await argon.hash(dto.password);

      const myCuid = cuid();
      const myDate = new Date(); // Your timezone!
      const myEpoch = Math.floor(myDate.getTime() / 1000.0);
      //save the new user in the db
      const user = await this.prisma.users.create({
        data: {
          id: myCuid,
          email: dto.email,
          created_at: myEpoch,
          updated_at: myEpoch,
          password: hash,
        },
      });
      if (!user) {
        const response = {
          execution: false,
          stack: 'register',
          message: {
            message: 'Failed to register',
            status_code: 500,
          },
          data: {},
        };
        return response;
      }
      const storeInfo = await this.storeUserInfo(
        myCuid,
        dto.fullname,
        dto.phone,
      );
      if (!storeInfo) {
        const response = {
          execution: false,
          stack: 'storeInfo',
          message: {
            message: 'Failed to register',
            status_code: 500,
          },
          data: {},
        };
        return response;
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Success register user',
          status_code: 204,
        },
        data: {},
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async signin(dto: SignInDto): Promise<StandardResponse> {
    try {
      const user = await this.prisma.users.findFirst({
        where: {
          AND: [
            {
              email: {
                equals: dto.email,
              },
            },
            {
              status: { equals: true },
            },
          ],
        },
      });
      if (!user) {
        const response = {
          execution: false,
          stack: 'FindUser',
          message: {
            message: 'User Is Not Found',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }
      const pwMatch = await argon.verify(user.password, dto.password);

      if (!pwMatch) {
        const response = {
          execution: false,
          stack: 'pwMatch',
          message: {
            message: 'Password Is Incorrect',
            status_code: 401,
          },
          data: {},
        };
        return response;
      }
      const token = await this.tokenJWT(user.id);
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Generate Successfull',
          status_code: 200,
        },
        data: token,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async tokenJWT(userId: string): Promise<{ access_token: string }> {
    const payload = {
      sub: userId,
    };

    const secret = this.config.get('JWT_SECRET');

    const token = await this.jwt.signAsync(payload, {
      expiresIn: '15m',
      secret: secret,
    });
    return { access_token: `${token}` };
  }
  private async storeUserInfo(
    user_id: string,
    fullname: string,
    phone: string,
  ) {
    try {
      const post = await this.prisma.user_info.create({
        data: {
          auth_id: user_id,
          fullname: fullname,
          phone: phone,
        },
      });
      if (post) {
        return post;
      }
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
}
