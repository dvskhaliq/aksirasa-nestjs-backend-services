import {
  Controller,
  Post,
  Body,
  Version,
  HttpCode,
  UseGuards,
  HttpException,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { SignUpDto, SignInDto } from './dto';
import { PublicGuard } from 'src/common/guards/public.guard';

@Controller('auth')
@UseGuards(PublicGuard)
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @HttpCode(204)
  @Version('1')
  @Post('signup')
  async signup(@Body() dto: SignUpDto) {
    const SignUp = await this.authService.signup(dto);
    if (SignUp.execution === true && SignUp.stack === 'done') {
      return SignUp;
    } else {
      throw new HttpException(
        SignUp.message.message,
        SignUp.message.status_code,
      );
    }
  }

  @HttpCode(200)
  @Version('1')
  @Post('signin')
  async signin(@Body() dto: SignInDto) {
    const signInResult = await this.authService.signin(dto);
    if (signInResult.execution === true && signInResult.stack === 'done') {
      return signInResult;
    } else {
      throw new HttpException(
        signInResult.message.message,
        signInResult.message.status_code,
      );
    }
  }
}
