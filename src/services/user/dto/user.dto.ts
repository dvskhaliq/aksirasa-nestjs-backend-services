import { IsNotEmpty, IsString } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @IsString()
  nama_lengkap: string;

  @IsNotEmpty()
  @IsString()
  phone: string;

  @IsString()
  alamat_lengkap: string;
}
