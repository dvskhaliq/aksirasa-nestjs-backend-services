import {
  Controller,
  Get,
  Body,
  Req,
  UseGuards,
  Version,
  HttpException,
  Put,
  UseInterceptors,
  BadRequestException,
  UploadedFile,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/index';
// import { AuthGuardsMiddleware } from 'src/middleware/AuthGuards.middleware';
import { FileInterceptor } from '@nestjs/platform-express';
// import { diskStorage } from 'multer';
import { Request } from 'express';
import { AuthGuard } from 'src/common/guards/auth.guard';

@Controller('user')
@UseGuards(AuthGuard)
export class UserController {
  constructor(private readonly userService: UserService) {}

  //Get Controller
  @Version('1')
  @Get('/info/get')
  // @UseGuards(AuthGuardsMiddleware)
  async getDetailUser(@Req() req: Request) {
    const userData = req['user'];
    const getDetailUser = await this.userService.getDetailUser(userData);
    if (getDetailUser.execution === true && getDetailUser.stack === 'done') {
      return getDetailUser;
    } else {
      throw new HttpException(
        getDetailUser.message.message,
        getDetailUser.message.status_code,
      );
    }
  }

  @Version('1')
  @Put('/info/update')
  // @UseGuards(AuthGuardsMiddleware)
  @UseInterceptors(
    FileInterceptor('pp', {
      // storage: diskStorage({
      //   destination: './clients/img',
      //   filename: (req, file, callback) => {
      //     const filename = file.originalname;
      //     callback(null, filename);
      //   },
      // }),
      // Konfigurasi untuk validasi ukuran dan tipe file
      fileFilter: (req, file, callback) => {
        const allowedMimeTypes = ['image/jpeg', 'image/jpg', 'image/png'];
        if (!allowedMimeTypes.includes(file.mimetype)) {
          return callback(
            new BadRequestException(
              'Invalid file format. Only JPEG, JPG, and PNG are allowed.',
            ),
            false,
          );
        }

        const maxSize = 1 * 1024 * 1024; // 5MB
        if (file.size > maxSize) {
          return callback(
            new BadRequestException(
              'File size exceeds the limit. Max size is 5MB.',
            ),
            false,
          );
        }

        callback(null, true);
      },
    }),
  )
  async updateDetailUser(
    @Req() req: Request,
    @UploadedFile() pp: Express.Multer.File,
    @Body() UserDto: CreateUserDto,
  ) {
    const userData = req['user'];
    const put = await this.userService.putUser(userData, UserDto, pp);
    if (put.execution === true && put.stack === 'done') {
      return put;
    } else {
      throw new HttpException(put.message.message, put.message.status_code);
    }
  }
}
