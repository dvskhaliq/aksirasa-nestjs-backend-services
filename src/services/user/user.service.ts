import { Injectable, Logger } from '@nestjs/common';
import { CreateUserDto } from './dto/index';
import { PrismaService } from 'src/prisma/prisma.service';
import { ConfigService } from '@nestjs/config';
import { PrismaClientExceptionFilter } from 'src/common/prisma-client-exception/prisma-client-exception.filter';
import * as fs from 'fs';
import cuid = require('cuid');
import path = require('path');

@Injectable()
export class UserService {
  private logger = new Logger(UserService.name);
  constructor(
    private prisma: PrismaService,
    private config: ConfigService,
  ) {}

  async getDetailUser(userData: any) {
    try {
      const fetchEmail = await this.prisma.users.findFirst({
        where: { id: userData.userId },
      });
      const getDetailUser = await this.prisma.user_info.findFirst({
        where: { auth_id: userData.userId },
      });
      if (getDetailUser === null) {
        const response = {
          execution: false,
          stack: 'Find User',
          message: {
            message: 'User Is Not Found',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }
      const getPP = await this.getPPUser(getDetailUser.id);
      const getUser = {
        id: getDetailUser.id,
        email: fetchEmail.email,
        fullname: getDetailUser.fullname,
        phone: getDetailUser.phone,
        address: getDetailUser.address,
        profile: getPP,
      };
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull find organization activity',
          status_code: 200,
        },
        data: getUser,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async putUser(
    userData: any,
    UserDto: CreateUserDto,
    pp: Express.Multer.File,
  ) {
    try {
      const findUser = await this.prisma.user_info.findFirst({
        where: { auth_id: userData.userId },
      });
      if (findUser == null) {
        const myCuid = cuid();
        const create = await this.prisma.user_info.create({
          data: {
            id: myCuid,
            auth_id: userData.userId,
            fullname: UserDto.nama_lengkap,
            address: UserDto.alamat_lengkap,
            phone: UserDto.phone,
          },
        });
        if (!create) {
          const response = {
            execution: false,
            stack: 'createInfo',
            message: {
              message: 'Cannot update user info',
              status_code: 500,
            },
            data: {},
          };
          return response;
        }
        if (pp) {
          // cek existing file name
          await this.postPP(myCuid, pp);
        }
        //return response to controller
        const response = {
          execution: true,
          stack: 'done',
          message: {
            message: 'Successfull update user info',
            status_code: 200,
          },
          data: {},
        };
        return response;
      } else {
        const update = await this.prisma.user_info.update({
          where: { id: findUser.id },
          data: {
            fullname: UserDto.nama_lengkap,
            address: UserDto.alamat_lengkap,
            phone: UserDto.phone,
          },
        });
        if (!update) {
          const response = {
            execution: false,
            stack: 'imglist',
            message: {
              message: 'Cannot update user info',
              status_code: 500,
            },
            data: {},
          };
          return response;
        }
        // const putAddress = await this.prisma.user_address.update({
        //   where: { info_id: findUser.id },
        //   data: {
        //     address: UserDto.alamat_lengkap,
        //   },
        // });
        if (pp) {
          // cek existing file name
          await this.postPP(findUser.id, pp);
        }
        //return response to controller
        const response = {
          execution: true,
          stack: 'done',
          message: {
            message: 'Successfull update user info',
            status_code: 200,
          },
          data: {},
        };
        return response;
      }
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async postPP(userInfo_id: string, img: any) {
    try {
      const cekExistingpp = await this.prisma.image_list.findFirst({
        where: {
          AND: [
            {
              user_id: { equals: userInfo_id },
            },
            {
              command: { equals: 'User Profile' },
            },
          ],
        },
      });
      if (cekExistingpp == null) {
        const name = img.originalname.split('.')[0];
        const fileExtension = path.extname(img.originalname);
        const newFileName = `${name}-${userInfo_id}${fileExtension}`;
        const createPP = await this.prisma.image_list.create({
          data: {
            user_id: userInfo_id,
            name_file: newFileName,
            command: 'User Profile',
          },
        });
        if (!createPP) {
          const response = {
            execution: false,
            stack: 'img',
            message: {
              message: 'Cannot update profile user',
              status_code: 500,
            },
            data: {},
          };
          return response;
        }
        const filePath = path.join('./clients/img', newFileName);
        // const filePath = `./clients/img/${pp.originalname}`; /* Path untuk menyimpan file pertama */
        fs.writeFileSync(filePath, img.buffer);
      } else {
        const name = img.originalname.split('.')[0];
        const fileExtension = path.extname(img.originalname);
        const newFileName = `${name}-${cekExistingpp.id}${fileExtension}`;
        const updatePP = await this.prisma.image_list.update({
          where: { id: cekExistingpp.id },
          data: {
            name_file: newFileName,
          },
        });
        if (!updatePP) {
          const response = {
            execution: false,
            stack: 'img',
            message: {
              message: 'Cannot update profile user',
              status_code: 500,
            },
            data: {},
          };
          return response;
        }
        // cek return data is not null unlink older file
        const oldImagePathCover = path.join(
          './clients/img',
          cekExistingpp.name_file,
        );
        if (fs.existsSync(oldImagePathCover)) {
          fs.unlinkSync(oldImagePathCover);
        }
        // change file
        const filePath = path.join('./clients/img', newFileName);
        fs.writeFileSync(filePath, img.buffer);
      }
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async getPPUser(user_id: string) {
    const getPP = await this.prisma.image_list.findFirst({
      where: {
        AND: [
          {
            user_id: { equals: user_id },
          },
          {
            command: { equals: 'User Profile' },
          },
        ],
      },
    });
    let Url: object;
    const base = this.config.get<string>('NEST_BASEURL');
    if (getPP == null) {
      Url = { url: null };
    } else {
      Url = {
        url: `${base}img/${getPP.name_file}`,
      };
    }
    return Url;
  }
}
