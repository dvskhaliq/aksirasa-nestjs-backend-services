import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Version,
  UseGuards,
  HttpException,
  UseInterceptors,
  UploadedFiles,
  Put,
  HttpCode,
  Query,
} from '@nestjs/common';
import { ProductService } from './product.service';
import {
  CreateProductDto,
  RatingProductDto,
  UpdateRatingProductDto,
  UpdateRatingLikeProductDto,
  QueryParamProductALL,
  QueryParamImageProduct,
} from './dto/index';
// import { AuthGuardsMiddleware } from 'src/middleware/AuthGuards.middleware';
import { FilesInterceptor } from '@nestjs/platform-express';
import { LimitFilesInterceptor } from 'src/common/interceptor/file-limits';
import { AuthGuard } from 'src/common/guards/auth.guard';
import { PublicGuard } from 'src/common/guards/public.guard';

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  // GET Controller
  @Version('1')
  @Get('/catalog/totalitem')
  @UseGuards(PublicGuard)
  async countTotalItem() {
    const getProduct = await this.productService.getTotalItem();
    if (getProduct.execution === true && getProduct.stack === 'done') {
      return getProduct;
    } else {
      throw new HttpException(
        getProduct.message.message,
        getProduct.message.status_code,
      );
    }
  }
  @Version('1')
  @Get('/catalog')
  @UseGuards(PublicGuard)
  async getCatalog(@Query() queryParam: QueryParamProductALL) {
    const page = parseInt(queryParam.page, 10);
    const take = parseInt(queryParam.take, 10);
    let order: boolean;
    if (
      queryParam.order === 'True' ||
      queryParam.order === 'true' ||
      queryParam.order === 'TRUE'
    ) {
      order = true;
    } else {
      order = false;
    }
    let skip: number;
    if (page > 1) {
      skip = (page - 1) * take;
    } else {
      skip = 0;
    }
    const getProduct = await this.productService.getCatalog(skip, take, order);
    if (getProduct.execution === true && getProduct.stack === 'done') {
      return getProduct;
    } else {
      throw new HttpException(
        getProduct.message.message,
        getProduct.message.status_code,
      );
    }
  }
  @Version('1')
  @Get('/catalog/byParam')
  @UseGuards(PublicGuard)
  async getOrganizationByParam(
    @Query() queryParam: { keyword?: string; category?: string },
  ) {
    const keyword = queryParam.keyword;
    const category = queryParam.category;
    const getAll = await this.productService.findProductParam(
      keyword,
      category,
    );
    if (getAll.execution === true && getAll.stack === 'done') {
      return getAll;
    } else {
      throw new HttpException(
        getAll.message.message,
        getAll.message.status_code,
      );
    }
  }

  //Post Product
  @Version('1')
  @Post('/post/:yayasan_id')
  @UseGuards(AuthGuard)
  @UseInterceptors(FilesInterceptor('image'), new LimitFilesInterceptor(5))
  async postProduct(
    @Param('yayasan_id') yayasan_id: string,
    @Body() productDto: CreateProductDto,
    @UploadedFiles() image: Array<Express.Multer.File>,
  ) {
    const postProduct = await this.productService.postProduct(
      yayasan_id,
      productDto,
      image,
    );
    if (postProduct.execution === true && postProduct.stack === 'done') {
      return postProduct;
    } else {
      throw new HttpException(
        postProduct.message.message,
        postProduct.message.status_code,
      );
    }
  }

  @Version('1')
  @Post('/rating/post/:product_id')
  @UseGuards(AuthGuard)
  @UseInterceptors(FilesInterceptor('image'), new LimitFilesInterceptor(5))
  async postRating(
    @Param('product_id') product_id: string,
    @Body() RatingDto: RatingProductDto,
    @UploadedFiles() image: Array<Express.Multer.File>,
  ) {
    const postRating = await this.productService.postRating(
      product_id,
      RatingDto,
      image,
    );
    if (postRating.execution === true && postRating.stack === 'done') {
      return postRating;
    } else {
      throw new HttpException(
        postRating.message.message,
        postRating.message.status_code,
      );
    }
  }

  //Put Product
  @Version('1')
  @Put('/rating/update/like/:rating_id')
  @UseGuards(AuthGuard)
  async postLike(
    @Param('rating_id') rating_id: string,
    @Body() RatingDto: UpdateRatingLikeProductDto,
  ) {
    const put = await this.productService.putLike(rating_id, RatingDto);
    if (put.execution === true && put.stack === 'done') {
      return put;
    } else {
      throw new HttpException(put.message.message, put.message.status_code);
    }
  }
  @Version('1')
  @Put('/rating/update/:rating_id')
  @UseGuards(AuthGuard)
  async putRating(
    @Param('rating_id') rating_id: string,
    @Body() updateDto: UpdateRatingProductDto,
  ) {
    const putRating = await this.productService.putRating(rating_id, updateDto);
    if (putRating.execution === true && putRating.stack === 'done') {
      return putRating;
    } else {
      throw new HttpException(
        putRating.message.message,
        putRating.message.status_code,
      );
    }
  }
  @Version('1')
  @Put('/update/:product_id')
  @UseGuards(AuthGuard)
  async putProduct(
    @Param('product_id') product_id: string,
    @Body() updateDto: CreateProductDto,
  ) {
    const update = await this.productService.putProduct(product_id, updateDto);
    if (update.execution === true && update.stack === 'done') {
      return update;
    } else {
      throw new HttpException(
        update.message.message,
        update.message.status_code,
      );
    }
  }

  @Version('1')
  @Put('/image/update')
  @UseGuards(AuthGuard)
  @UseInterceptors(FilesInterceptor('image'), new LimitFilesInterceptor(1))
  async putGallery(
    @Query() queryParam: QueryParamImageProduct,
    @UploadedFiles() image: Array<Express.Multer.File>,
  ) {
    const product_id = queryParam.product_id;
    const updateImage = await this.productService.putImage(product_id, image);
    if (updateImage.execution === true && updateImage.stack === 'done') {
      return updateImage;
    } else {
      throw new HttpException(
        updateImage.message.message,
        updateImage.message.status_code,
      );
    }
  }

  //Delete Controller
  @Version('1')
  @Delete('/image/delete/:image_id')
  @UseGuards(AuthGuard)
  @HttpCode(204)
  async delateImage(@Param('image_id') image_id: string) {
    const deleteImage = await this.productService.imageDelete(image_id);
    if (deleteImage.execution === true && deleteImage.stack === 'done') {
      return deleteImage;
    } else {
      throw new HttpException(
        deleteImage.message.message,
        deleteImage.message.status_code,
      );
    }
  }

  @Version('1')
  @Delete('/delete/:product_id')
  @UseGuards(AuthGuard)
  @HttpCode(204)
  async deleteProduct(@Param('product_id') product_id: string) {
    const deleteProduct = await this.productService.deleteProduct(product_id);
    if (deleteProduct.execution === true && deleteProduct.stack === 'done') {
      return deleteProduct;
    } else {
      throw new HttpException(
        deleteProduct.message.message,
        deleteProduct.message.status_code,
      );
    }
  }
}
