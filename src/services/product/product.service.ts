import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import {
  CreateProductDto,
  RatingProductDto,
  UpdateRatingProductDto,
  UpdateRatingLikeProductDto,
} from './dto/index';
import { PrismaService } from 'src/prisma/prisma.service';
import { ConfigService } from '@nestjs/config';
import { PrismaClientExceptionFilter } from 'src/common/prisma-client-exception/prisma-client-exception.filter';
import path = require('path');
import cuid = require('cuid');
import * as fs from 'fs';
import { StandardResponse } from 'src/types';

@Injectable()
export class ProductService {
  private logger = new Logger(ProductService.name);
  constructor(
    private prisma: PrismaService,
    private config: ConfigService,
  ) {}

  async getTotalItem() {
    try {
      const getCatalog = await this.prisma.product_organisasi.count();
      if (!getCatalog) {
        const response = {
          execution: false,
          stack: 'getCatalog',
          message: {
            message: 'No data found',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }
      const total = getCatalog;
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull found data',
          status_code: 200,
        },
        data: total,
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async getCatalog(
    skip: number,
    take: number,
    order: boolean,
  ): Promise<StandardResponse> {
    try {
      if (order === false) {
        const getCatalog = await this.prisma.product_organisasi.findMany({
          skip: skip,
          take: take,
        });
        if (!getCatalog) {
          const response = {
            execution: false,
            stack: 'getCatalog',
            message: {
              message: 'No data found',
              status_code: 404,
            },
            data: {},
          };
          return response;
        }
        const data: object[] = [];
        for (const product of getCatalog) {
          const processImage = await this.findImage(product.id);
          const prosesRating = await this.findRate(product.id);
          const Owner = await this.findOwnerProduct(product.yayasan_id);
          const processedData = {
            id: product.id,
            yayasan_id: product.yayasan_id,
            judul_product: product.judul_product,
            harga: product.harga,
            descripsion: product.description_product,
            tanggal_post: product.tanggal_post,
            image: processImage,
            rating: prosesRating,
            owner: Owner,
          };
          data.push(processedData);
        }
        const response = {
          execution: true,
          stack: 'done',
          message: {
            message: 'Sucessfull found data',
            status_code: 200,
          },
          data: data,
        };
        return response;
      } else {
        const getCatalog = await this.prisma.product_organisasi.findMany({
          skip: skip,
          take: take,
          orderBy: {
            tanggal_post: 'desc',
          },
        });
        if (!getCatalog) {
          const response = {
            execution: false,
            stack: 'getCatalog',
            message: {
              message: 'No data found',
              status_code: 404,
            },
            data: {},
          };
          return response;
        }
        const data: object[] = [];
        for (const product of getCatalog) {
          const processImage = await this.findImage(product.id);
          const prosesRating = await this.findRate(product.id);
          const Owner = await this.findOwnerProduct(product.yayasan_id);
          const processedData = {
            id: product.id,
            yayasan_id: product.yayasan_id,
            judul_product: product.judul_product,
            harga: product.harga,
            descripsion: product.description_product,
            tanggal_post: product.tanggal_post,
            image: processImage,
            rating: prosesRating,
            owner: Owner,
          };
          data.push(processedData);
        }
        const response = {
          execution: true,
          stack: 'done',
          message: {
            message: 'Sucessfull found data',
            status_code: 200,
          },
          data: data,
        };
        return response;
      }
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async findProductParam(
    keyword: string,
    category: string,
  ): Promise<StandardResponse> {
    try {
      const data: object[] = [];
      if (keyword && !category) {
        const productKeyword = await this.prisma.product_organisasi.findMany({
          where: {
            judul_product: { contains: keyword },
          },
        });
        if (productKeyword) {
          for (const product of productKeyword) {
            const processImage = await this.findImage(product.id);
            const prosesRating = await this.findRate(product.id);
            const Owner = await this.findOwnerProduct(product.yayasan_id);
            const processedData = {
              id: product.id,
              yayasan_id: product.yayasan_id,
              judul_product: product.judul_product,
              harga: product.harga,
              descripsion: product.description_product,
              image: processImage,
              rating: prosesRating,
              owner: Owner,
            };
            data.push(processedData);
          }
        }
      } else if (category && !keyword) {
        const productKeyword = await this.prisma.product_organisasi.findMany({
          where: {
            category: { equals: category },
          },
        });
        if (productKeyword) {
          for (const product of productKeyword) {
            const processImage = await this.findImage(product.id);
            const prosesRating = await this.findRate(product.id);
            const Owner = await this.findOwnerProduct(product.yayasan_id);
            const processedData = {
              id: product.id,
              yayasan_id: product.yayasan_id,
              judul_product: product.judul_product,
              harga: product.harga,
              descripsion: product.description_product,
              image: processImage,
              rating: prosesRating,
              owner: Owner,
            };
            data.push(processedData);
          }
        }
      } else {
        const productKeyword = await this.prisma.product_organisasi.findMany({
          where: {
            OR: [
              {
                category: { equals: category },
                judul_product: { contains: keyword },
              },
            ],
          },
        });
        if (productKeyword) {
          for (const product of productKeyword) {
            const processImage = await this.findImage(product.id);
            const prosesRating = await this.findRate(product.id);
            const Owner = await this.findOwnerProduct(product.yayasan_id);
            const processedData = {
              id: product.id,
              yayasan_id: product.yayasan_id,
              judul_product: product.judul_product,
              harga: product.harga,
              descripsion: product.description_product,
              image: processImage,
              rating: prosesRating,
              owner: Owner,
            };
            data.push(processedData);
          }
        }
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Generate sucessfull',
          status_code: 200,
        },
        data: data,
      };
      console.log(response);
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }

  //Post Services
  async postRating(
    product_id: string,
    RatingDto: RatingProductDto,
    image: Express.Multer.File[],
  ) {
    try {
      const myCuid = await this.generateCuid();
      const postRating = await this.prisma.product_rating.create({
        data: {
          id: myCuid,
          product_id: product_id,
          product_rating: +RatingDto.product_rating,
          comment: RatingDto.comment,
          like: 0,
          dislike: 0,
        },
      });
      if (!postRating) {
        const response = {
          execution: false,
          stack: 'post',
          message: {
            message: 'Failed to post rating',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }
      if (image.length > 0) {
        const save_file = await this.save_imgRating(image, myCuid);
        if (!save_file) {
          const response = {
            execution: false,
            stack: 'saveImage',
            message: {
              message: 'Failed to save image',
              status_code: 500,
            },
            data: {},
          };
          return response;
        }
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull create product',
          status_code: 201,
        },
        data: {},
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async postProduct(
    yayasan_id: string,
    productDto: CreateProductDto,
    image: Express.Multer.File[],
  ) {
    try {
      //cek if file upload if exist save them
      const myCuid = await this.generateCuid();
      const myDate = new Date(); // Your timezone!
      const myEpoch = Math.floor(myDate.getTime() / 1000.0);
      const postProduct = await this.prisma.product_organisasi.create({
        data: {
          id: myCuid,
          yayasan_id: yayasan_id,
          judul_product: productDto.judul_product,
          harga: productDto.harga,
          description_product: productDto.description_product,
          category: productDto.category,
          tanggal_post: myEpoch,
        },
      });
      if (!postProduct) {
        const response = {
          execution: false,
          stack: 'post',
          message: {
            message: 'Failed to post Product',
            status_code: 500,
          },
          data: {},
        };
        return response;
      }
      if (image.length > 0) {
        const creteimage = await this.save_imgGallery(image, myCuid);
        if (!creteimage) {
          const response = {
            execution: false,
            stack: 'saveGallery',
            message: {
              message: 'Failed to save image',
              status_code: 500,
            },
            data: {},
          };
          return response;
        }
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull create product',
          status_code: 200,
        },
        data: {},
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }

  //Put Services
  async putLike(rating_id: string, updatelike: UpdateRatingLikeProductDto) {
    try {
      const put = await this.prisma.product_rating.update({
        where: { id: rating_id },
        data: {
          like: updatelike.like,
          dislike: updatelike.dislike,
        },
      });
      if (!put) {
        const response = {
          execution: false,
          stack: 'post',
          message: {
            message: 'Failed to put rating product',
            status_code: 500,
          },
          data: {},
        };
        return response;
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull update product',
          status_code: 200,
        },
        data: {},
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async putRating(rating_id: string, UpdateRatingDto: UpdateRatingProductDto) {
    try {
      const put = this.prisma.product_rating.update({
        where: { id: rating_id },
        data: {
          product_rating: UpdateRatingDto.product_rating,
          comment: UpdateRatingDto.comment,
        },
      });
      if (!put) {
        const response = {
          execution: false,
          stack: 'post',
          message: {
            message: 'Failed to put rating product',
            status_code: 500,
          },
          data: {},
        };
        return response;
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull update product',
          status_code: 200,
        },
        data: {},
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async putProduct(product_id: string, updateDto: CreateProductDto) {
    try {
      const putProduct = await this.prisma.product_organisasi.update({
        where: { id: product_id },
        data: {
          judul_product: updateDto.judul_product,
          harga: updateDto.harga,
          description_product: updateDto.description_product,
        },
      });
      if (!putProduct) {
        const response = {
          execution: false,
          stack: 'putProduct',
          message: {
            message: 'Failed to update product',
            status_code: 500,
          },
          data: {},
        };
        return response;
      }
      const response = {
        execution: true,
        stack: 'done',
        message: {
          message: 'Sucessfull update product',
          status_code: 200,
        },
        data: {},
      };
      return response;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }

  async putImage(product_id: string, image: Express.Multer.File[]) {
    try {
      const save_image = await this.save_imgGallery(image, product_id);
      if (!save_image) {
        const response = {
          execution: false,
          stack: 'putGallery',
          message: {
            message: 'Failed to update image',
            status_code: 500,
          },
          data: {},
        };
        return response;
      } else {
        const response = {
          execution: true,
          stack: 'done',
          message: {
            message: 'Successfull update image',
            status_code: 204,
          },
          data: {},
        };
        return response;
      }
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async imageDelete(image_id: string) {
    try {
      const findImage = await this.prisma.product_gallery.findFirst({
        where: { id: image_id },
      });
      if (findImage != null) {
        const deleteImage = await this.prisma.product_gallery.delete({
          where: { id: image_id },
        });
        // Hapus file Organization Cover lama dari direktori ./client/img
        const oldImagePathCover = path.join(
          './clients/img',
          deleteImage.name_file,
        );
        // const oldImagePathCover = path.join(__dirname, `./clients/img/`);
        if (fs.existsSync(oldImagePathCover)) {
          fs.unlinkSync(oldImagePathCover);
        }
        const response = {
          execution: true,
          stack: 'done',
          message: {
            message: 'Successfull delete image',
            status_code: 204,
          },
          data: {},
        };
        return response;
      } else {
        const response = {
          execution: false,
          stack: 'findImage',
          message: {
            message: 'Failed to delete image product, Image Not Found',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  async deleteProduct(product_id: string) {
    try {
      const findProduct = await this.prisma.product_organisasi.findFirst({
        where: { id: product_id },
      });
      if (findProduct != null) {
        const findImage = await this.findImage(product_id);
        if (findImage != null) {
          for (const image of findImage) {
            const deleteImage = await this.prisma.product_gallery.delete({
              where: { id: image.id },
            });
            if (deleteImage) {
              // Hapus file Organization Cover lama dari direktori ./client/img
              const oldImagePathCover = path.join(
                './clients/img',
                image.name_file,
              );
              // const oldImagePathCover = path.join(__dirname, `./clients/img/`);
              if (fs.existsSync(oldImagePathCover)) {
                fs.unlinkSync(oldImagePathCover);
              }
            }
          }
        }
        const deleteProduct = await this.prisma.product_organisasi.delete({
          where: { id: product_id },
        });
        if (!deleteProduct) {
          const response = {
            execution: false,
            stack: 'deleteproduct',
            message: {
              message: 'Failed to delete product',
              status_code: 500,
            },
            data: {},
          };
          return response;
        }
        const response = {
          execution: true,
          stack: 'done',
          message: {
            message: 'Successfull to delete product',
            status_code: 204,
          },
          data: {},
        };
        return response;
      } else {
        const response = {
          execution: false,
          stack: 'deleteproduct',
          message: {
            message: 'Failed to delete product, No Data Product Found',
            status_code: 404,
          },
          data: {},
        };
        return response;
      }
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  //proses array file to db
  private async findOwnerProduct(yayasan_id: string) {
    try {
      const findOrg = await this.prisma.yayasan.findFirst({
        where: { id: yayasan_id },
      });
      let Owner: object;
      let Url: object;
      if (findOrg) {
        const findtPP = await this.prisma.image_list.findFirst({
          where: {
            AND: [
              { yayasan_id: yayasan_id },
              { command: { contains: 'Organization Logo' } },
            ],
          },
        });
        const base = this.config.get<string>('NEST_BASEURL');
        if (findtPP == null) {
          Url = {};
        } else {
          Url = {
            id: findtPP.id,
            name_file: findtPP.name_file,
            link: `${base}img/${findtPP.name_file}`,
          };
        }
        Owner = {
          fullname_organisasi: findOrg.fullname_organisasi,
          provinsi: findOrg.provinsi,
          logo: Url,
        };
        return Owner;
      } else {
        Owner: {
        }
        return Owner;
      }
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async findRate(product_id: string) {
    try {
      const getRate = await this.prisma.product_rating.findMany({
        where: { product_id: product_id },
      });
      const data: object[] = [];
      for (const product of getRate) {
        const processImage = await this.finImageRate(product.id);
        const processedData = {
          id: product.id,
          product_id: product.product_id,
          product_rating: product.product_rating,
          comment: product.comment,
          like: product.like,
          dislike: product.dislike,
          image: processImage,
        };
        data.push(processedData);
      }
      return data;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async finImageRate(rate_id: string) {
    try {
      const base = this.config.get<string>('NEST_BASEURL');
      const getImage = await this.prisma.rating_image.findMany({
        where: {
          AND: [
            { rating_id: rate_id },
            { command: { contains: 'Rating Image for' } },
          ],
        },
      });
      const processImage: object[] = [];
      let Url: object;
      for (const Image of getImage) {
        if (Image == null) {
          Url = {};
        } else {
          Url = {
            id: Image.id,
            name_file: Image.name_file,
            link: `${base}img/${Image.name_file}`,
          };
        }
        processImage.push(Url);
      }
      return processImage;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async findImage(product_id: string) {
    try {
      const base = this.config.get<string>('NEST_BASEURL');
      const getImage = await this.prisma.product_gallery.findMany({
        where: { product_id: product_id },
      });
      const processImage = [];
      let Url: object;
      for (const Image of getImage) {
        if (Image == null) {
          Url = {};
        } else {
          Url = {
            id: Image.id,
            name_file: Image.name_file,
            link: `${base}img/${Image.name_file}`,
          };
        }
        processImage.push(Url);
      }
      return processImage;
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async save_imgRating(
    image: Express.Multer.File[],
    rating_id: string,
  ) {
    const savedFiles: any[] = [];
    const filePromises = image.map(async (file) => {
      // cek valid image ;
      const isValidFile = await this.isValidFile(file);
      if (!isValidFile) {
        throw new BadRequestException(
          `Invalid file extension for file '${file.originalname}'. Only JPG/JPEG are allowed.`,
        );
      }
      try {
        const name = file.originalname.split('.')[0];
        const fileExtension = path.extname(file.originalname);
        const newFileName = `${name}-${rating_id}${fileExtension}`;
        const filePath = path.join('./clients/img', newFileName);
        const savedFile = await this.prisma.rating_image.create({
          data: {
            rating_id: rating_id,
            name_file: newFileName,
            command: `Rating Image for ${rating_id}`,
          },
        });
        // Simpan file ke sistem file
        await fs.promises.writeFile(filePath, file.buffer);
        savedFiles.push(savedFile);
        return savedFile;
      } catch (err) {
        this.logger.log({
          level: 'error',
          message: 'Exeption Handler Looging for',
          err: err,
          errCustomCode: '20',
        });
        throw new PrismaClientExceptionFilter();
        // throw new BadRequestException('Failed to save file to database');
      }
    });
    return Promise.all(filePromises);
  }
  private async put_Image(
    product_id: string,
    image_id: string,
    image: Express.Multer.File,
  ) {
    try {
      const findProductImage = await this.prisma.product_gallery.findFirst({
        where: {
          AND: [{ id: image_id }, { product_id: product_id }],
        },
      });

      if (findProductImage !== null) {
        // Hapus file Organization Cover lama dari direktori ./client/img
        const oldImagePathProduct = path.join(
          './clients/img',
          findProductImage.name_file,
        );
        // const oldImagePathCover = path.join(__dirname, `./clients/img/`);
        if (fs.existsSync(oldImagePathProduct)) {
          fs.unlinkSync(oldImagePathProduct);
        }
        const name = image.originalname.split('.')[0];
        const fileExtension = path.extname(image.originalname);
        const newFileName = `${name}-${product_id}${fileExtension}`;
        const filePath = path.join('./clients/img', newFileName);

        //update image_list cover org
        const updateGalleryProduct = await this.prisma.product_gallery.update({
          where: { id: findProductImage.id },
          data: {
            name_file: newFileName,
          },
        });
        fs.writeFileSync(filePath, image.buffer);
        return updateGalleryProduct;
      } else {
        const name = image.originalname.split('.')[0];
        const fileExtension = path.extname(image.originalname);
        // Generate unique file name
        const fileName = `${name}-${product_id}${fileExtension}`;
        const savedFile = await this.prisma.product_gallery.create({
          data: {
            product_id: product_id,
            name_file: fileName,
            command: `Product Gallery for ${product_id}`,
          },
        });
        // Save file to server
        const uploadPath = path.join('./clients/img', fileName);
        await fs.promises.writeFile(uploadPath, image.buffer);
        return savedFile;
      }
    } catch (err) {
      this.logger.log({
        level: 'error',
        message: 'Exeption Handler Looging for',
        err: err,
        errCustomCode: '20',
      });
      throw new PrismaClientExceptionFilter();
    }
  }
  private async save_imgGallery(
    image: Express.Multer.File[],
    product_id: string,
  ) {
    const savedFiles: any[] = [];
    const filePromises = image.map(async (file) => {
      // cek valid image ;
      const isValidFile = await this.isValidFile(file);
      if (!isValidFile) {
        throw new BadRequestException(
          `Invalid file extension for file '${file.originalname}'. Only JPG/JPEG are allowed.`,
        );
      }
      try {
        const name = file.originalname.split('.')[0];
        const fileExtension = path.extname(file.originalname);
        const newFileName = `${name}-${product_id}${fileExtension}`;
        const filePath = path.join('./clients/img', newFileName);
        const savedFile = await this.prisma.product_gallery.create({
          data: {
            product_id: product_id,
            name_file: newFileName,
            command: `Product Gallery for ${product_id}`,
          },
        });
        // Simpan file ke sistem file
        await fs.promises.writeFile(filePath, file.buffer);
        savedFiles.push(savedFile);
        return savedFile;
      } catch (err) {
        this.logger.log({
          level: 'error',
          message: 'Exeption Handler Looging for',
          err: err,
          errCustomCode: '20',
        });
        throw new PrismaClientExceptionFilter();
        // throw new BadRequestException('Failed to save file to database');
      }
    });
    return Promise.all(filePromises);
  }
  private async isValidFile(file: Express.Multer.File): Promise<boolean> {
    if (!file) {
      return false;
    }

    const allowedExtensions = ['.jpg', '.jpeg', '.png'];
    const maxSizeInBytes = 1024 * 1024; // 1MB

    const fileExtension = path.extname(file.originalname).toLowerCase();
    if (!allowedExtensions.includes(fileExtension)) {
      return false;
    }

    if (file.size > maxSizeInBytes) {
      return false;
    }

    return true;
  }

  private async generateCuid(): Promise<string> {
    const Mycuid = cuid();
    return Mycuid;
  }
}
