import { IsNotEmpty, IsNumberString, IsString } from 'class-validator';

export class RatingProductDto {
  @IsNumberString()
  @IsNotEmpty()
  product_rating: number;

  @IsString()
  @IsNotEmpty()
  comment: string;
}
