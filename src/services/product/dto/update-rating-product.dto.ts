import { IsNumberString, IsString } from 'class-validator';

export class UpdateRatingProductDto {
  @IsNumberString()
  product_rating: number;

  @IsString()
  comment: string;
}
