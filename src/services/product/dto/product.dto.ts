import { IsNotEmpty, IsNumberString, IsString } from 'class-validator';

export class CreateProductDto {
  @IsString()
  @IsNotEmpty()
  judul_product: string;

  @IsNumberString()
  @IsNotEmpty()
  harga: number;

  @IsString()
  @IsNotEmpty()
  description_product: string;

  @IsString()
  @IsNotEmpty()
  category: string;
}

export class QueryParamImageProduct {
  @IsString()
  @IsNotEmpty()
  product_id: string;
}

export class QueryParamProductALL {
  @IsNotEmpty()
  @IsNumberString()
  page: string;

  @IsNotEmpty()
  @IsNumberString()
  take: string;

  @IsNotEmpty()
  @IsString()
  order: string;
}
