import { IsNumberString } from 'class-validator';

export class UpdateRatingLikeProductDto {
  @IsNumberString()
  like: number;

  @IsNumberString()
  dislike: number;
}
