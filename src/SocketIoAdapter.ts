import { INestApplicationContext, Logger } from '@nestjs/common';
import { IoAdapter } from '@nestjs/platform-socket.io';
import { ServerOptions } from 'socket.io';
import { ConfigService } from '@nestjs/config';

export class SocketIoAdapter extends IoAdapter {
  private readonly logger = new Logger(SocketIoAdapter.name);
  constructor(
    private app: INestApplicationContext,
    private configService: ConfigService,
  ) {
    super(app);
  }

  createIOServer(port: number, options?: ServerOptions) {
    const clientPort = parseInt(this.configService.get('CLIENT_PORT'));
    const clientIP = this.configService.get('CLIENT_DOMAIN');
    const cors = {
      origin: [
        `http://localhost:${clientPort}`,
        `http://${clientIP}:${clientPort}`,
        '192.168.56.1',
        '192.168.113.197',
        new RegExp(`/^http:\/\/192\.168\.1\.([1-9]|[1-9]\d):${clientPort}$/`),
      ],
    };

    this.logger.log('Configuring SocketIO server with custom CORS options', {
      cors,
    });
    this.logger.log({
      level: 'info',
      message: 'Configuring SocketIO server with custom CORS options',
      data: { cors: cors },
      errCustomCode: '20',
    });

    const optionsWithCORS: ServerOptions = {
      ...options,
      cors,
    };

    // we need to return this, even though the signature says it returns void
    return super.createIOServer(port, optionsWithCORS);
  }
}
