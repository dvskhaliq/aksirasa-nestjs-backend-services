import { Injectable } from '@nestjs/common';
import { decodedDto } from './dto/create-poll.dto';
// import { CreatePollDto } from './dto/create-poll.dto';
// import { UpdatePollDto } from './dto/update-poll.dto';

@Injectable()
export class PollsService {
  create(payload: any) {
    return `This action adds a new poll ${payload}`;
  }
  async initToken(decoded: decodedDto) {
    const user = {
      userId: decoded.sub,
      issuedAt: new Date(decoded.iat * 1000),
      expiresAt: new Date(decoded.exp * 1000),
    };
    return user;
  }
}
