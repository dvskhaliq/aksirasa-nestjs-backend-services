import {
  WebSocketGateway,
  OnGatewayInit,
  OnGatewayConnection,
  OnGatewayDisconnect,
  WebSocketServer,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { PollsService } from './polls.service';
import { HttpException, Logger } from '@nestjs/common';
import { ConnectedSocket } from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { interval, switchMap } from 'rxjs';
import { ProductService } from 'src/services/product/product.service';
import { OrganizationService } from 'src/services/organization/organization.service';
// import { TransformInterceptor } from 'src/common/transform/transform.interceptor';
import { JwtService } from '@nestjs/jwt';
import { AuthorizationDto } from './dto/create-poll.dto';

@WebSocketGateway({
  namespace: 'gateway',
})
// @UseInterceptors(TransformInterceptor) // Terapkan di level Gateway
export class PollsGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  private logger = new Logger(PollsGateway.name);
  constructor(
    private readonly pollsService: PollsService,
    private readonly jwtService: JwtService,
    private readonly productService: ProductService,
    private readonly organizationService: OrganizationService,
  ) {}

  @WebSocketServer() server: Server;
  afterInit(): void {
    this.logger.log({
      level: 'info',
      message: 'Websocket Gateway Initialized ',
      err: {},
      errCustomCode: '20',
    });
  }
  handleConnection(@ConnectedSocket() client: Socket) {
    this.logger.log({
      level: 'info',
      message: `Websocket Clients with id: ${client.id} Connected`,
      err: {},
      errCustomCode: '20',
    });
    this.getAllDataProduct();
  }
  handleDisconnect(client: Socket) {
    this.logger.log({
      level: 'info',
      message: `Disconnected Client to websocket with id: ${client.id} `,
      err: {},
      errCustomCode: '20',
    });
  }
  @SubscribeMessage('test')
  handleMessage(client: any, payload: any): object {
    const response = {
      execution: true,
      stack: 'done',
      message: {
        message: `Selamat datang ${client}`,
        status_code: 204,
      },
      data: payload,
    };
    return response;
  }

  @SubscribeMessage('yayasan_info')
  async sendBackYayasan_info(
    @ConnectedSocket() client: Socket,
    @MessageBody() payload: AuthorizationDto,
  ) {
    try {
      if (!client) {
        this.logger.error('Client is undefined');
        return;
      }
      const token = payload.token.startsWith('Bearer ')
        ? payload.token.slice(7)
        : payload.token;
      const decodedToken = this.jwtService.verify(token);
      const userData = this.pollsService.initToken(decodedToken);
      const getOne =
        await this.organizationService.find_DetailCharity(userData);
      if (getOne.execution === true && getOne.stack === 'done') {
        return client.emit('yayasan_info', getOne);
      } else {
        throw new HttpException(
          getOne.message.message,
          getOne.message.status_code,
        );
      }
    } catch (err) {
      this.logger.log({
        level: 'info',
        message: `Error Handling for websocket `,
        err: err,
        errCustomCode: '20',
      });
    }
  }

  // Event Controller
  async getAllDataProduct() {
    const skip = 0;
    const take = 5;
    const order = false;
    try {
      interval(3000000) // Polling setiap 5 menit
        .pipe(
          switchMap(
            async () => await this.productService.getCatalog(skip, take, order),
          ), // Mengambil data produk dari database
        )
        .subscribe((products) => {
          this.server.emit('products', products); // Mengirimkan data produk ke semua client
        });
    } catch (err) {
      this.logger.log({
        level: 'info',
        message: `Error Handling for websocket `,
        err: err,
        errCustomCode: '20',
      });
    }
  }
}
