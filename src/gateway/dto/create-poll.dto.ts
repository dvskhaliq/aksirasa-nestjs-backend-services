import { IsNotEmpty, IsString } from 'class-validator';

export class AuthorizationDto {
  @IsNotEmpty()
  @IsString()
  token: string;
}

export class decodedDto {
  @IsNotEmpty()
  @IsString()
  sub: string;

  @IsNotEmpty()
  @IsString()
  iat: number;

  @IsNotEmpty()
  @IsString()
  exp: number;
}
