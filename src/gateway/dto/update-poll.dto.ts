import { PartialType } from '@nestjs/mapped-types';
import { AuthorizationDto } from './create-poll.dto';

export class UpdatePollDto extends PartialType(AuthorizationDto) {
  id: number;
}
