import { Module } from '@nestjs/common';
import { PollsService } from './polls.service';
import { PollsGateway } from './polls.gateway';
import { ProductService } from 'src/services/product/product.service';
import { OrganizationService } from 'src/services/organization/organization.service';

@Module({
  imports: [],
  providers: [PollsGateway, PollsService, ProductService, OrganizationService],
})
export class PollsModule {}
