import {
  MiddlewareConsumer,
  Module,
  NestModule,
  // RequestMethod,
} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthController } from './services/auth/auth.controller';
import { AuthService } from './services/auth/auth.service';
import { PrismaModule } from './prisma/prisma.module';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './services/auth/auth.module';
import { JwtModule } from '@nestjs/jwt';
import { OrganizationModule } from './services/organization/organization.module';
// import { AuthGuardsMiddleware } from './middleware/AuthGuards.middleware';
import { APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { TransformInterceptor } from './common/transform/transform.interceptor';
import { MulterModule } from '@nestjs/platform-express';
// import { OrganizationController } from './services/organization/organization.controller';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { LoggingMiddleware } from './middleware/Looging.middleware';
import { UserModule } from './services/user/user.module';
// import { UserController } from './services/user/user.controller';
import { ProductModule } from './services/product/product.module';
// import { ProductController } from './services/product/product.controller';
import { TransactionModule } from './services/transaction/transaction.module';
// import { TransactionController } from './services/transaction/transaction.controller';
import { TripayService } from './http/tripay-services/tripay.service';
import { CommonServicesModule } from './services/common-services/common-services.module';
// import { CommonServicesController } from './services/common-services/common-services.controller';
import { RajaongkirServices } from './http/rajaongkir-services/rajaongkir.service';
import { PollsModule } from './gateway/polls.module';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    PrismaModule,
    JwtModule.register({
      global: true,
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '60m' }, // Atur opsi sesuai kebutuhan
    }),
    AuthModule,
    OrganizationModule,
    MulterModule.register({ dest: './clients' }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'clients'),
    }),
    ThrottlerModule.forRoot([
      {
        ttl: 60000,
        limit: 50,
      },
    ]),
    UserModule,
    ProductModule,
    TransactionModule,
    CommonServicesModule,
    PollsModule,
  ],
  controllers: [AppController, AuthController],
  providers: [
    AppService,
    AuthService,
    TripayService,
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
    RajaongkirServices,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    // consumer
    //   .apply(AuthGuardsMiddleware)
    //   .exclude(
    //     { path: 'organization/register', method: RequestMethod.POST },
    //     {
    //       path: 'transaction/tripay/callback_url',
    //       method: RequestMethod.POST,
    //     },
    //   )
    //   .forRoutes(
    //     OrganizationController,
    //     UserController,
    //     ProductController,
    //     TransactionController,
    //     CommonServicesController,
    //   );
    consumer.apply(LoggingMiddleware).forRoutes('*');
  }
}
