/*
  Warnings:

  - You are about to drop the column `product_id` on the `transaction_product` table. All the data in the column will be lost.
  - Added the required column `product_id` to the `order_item` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "transaction_product" DROP CONSTRAINT "transaction_product_product_id_fkey";

-- DropIndex
DROP INDEX "transaction_product_product_id_idx";

-- AlterTable
ALTER TABLE "order_item" ADD COLUMN     "product_id" VARCHAR(100) NOT NULL;

-- AlterTable
ALTER TABLE "transaction_product" DROP COLUMN "product_id";

-- CreateIndex
CREATE INDEX "order_item_product_id_idx" ON "order_item"("product_id");

-- AddForeignKey
ALTER TABLE "order_item" ADD CONSTRAINT "order_item_product_id_fkey" FOREIGN KEY ("product_id") REFERENCES "product_organisasi"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
