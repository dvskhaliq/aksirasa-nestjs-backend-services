/*
  Warnings:

  - You are about to drop the `category_product` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "category_product";

-- CreateTable
CREATE TABLE "product_category" (
    "id" SERIAL NOT NULL,
    "category_name" VARCHAR(100) NOT NULL,

    CONSTRAINT "product_category_pkey" PRIMARY KEY ("id")
);
