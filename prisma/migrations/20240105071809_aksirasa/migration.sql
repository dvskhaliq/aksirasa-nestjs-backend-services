/*
  Warnings:

  - Added the required column `references_code` to the `transaction_product` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "transaction_product" ADD COLUMN     "references_code" VARCHAR(100) NOT NULL;
