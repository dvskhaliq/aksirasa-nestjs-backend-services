-- AlterTable
CREATE SEQUENCE provinsi_id_seq;
ALTER TABLE "provinsi" ALTER COLUMN "id" SET DEFAULT nextval('provinsi_id_seq');
ALTER SEQUENCE provinsi_id_seq OWNED BY "provinsi"."id";
