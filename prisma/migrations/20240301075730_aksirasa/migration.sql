/*
  Warnings:

  - You are about to drop the `provinsi` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "provinsi";

-- CreateTable
CREATE TABLE "categoryProduct" (
    "id" SERIAL NOT NULL,
    "category_name" VARCHAR(100) NOT NULL,

    CONSTRAINT "categoryProduct_pkey" PRIMARY KEY ("id")
);
