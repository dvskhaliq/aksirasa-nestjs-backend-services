/*
  Warnings:

  - Added the required column `category` to the `product_organisasi` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "product_organisasi" ADD COLUMN     "category" VARCHAR(100) NOT NULL;
