/*
  Warnings:

  - Changed the type of `harga` on the `product_organisasi` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- AlterTable
ALTER TABLE "product_organisasi" DROP COLUMN "harga",
ADD COLUMN     "harga" MONEY NOT NULL;
