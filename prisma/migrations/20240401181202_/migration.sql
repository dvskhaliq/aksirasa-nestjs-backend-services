/*
  Warnings:

  - Added the required column `lokasi_alamat` to the `aktivitas_organisasi` table without a default value. This is not possible if the table is not empty.
  - Added the required column `lokasi_kecamatan` to the `aktivitas_organisasi` table without a default value. This is not possible if the table is not empty.
  - Added the required column `lokasi_kelurahan` to the `aktivitas_organisasi` table without a default value. This is not possible if the table is not empty.
  - Added the required column `lokasi_provinsi` to the `aktivitas_organisasi` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "aktivitas_organisasi" ADD COLUMN     "lokasi_alamat" TEXT NOT NULL,
ADD COLUMN     "lokasi_kecamatan" VARCHAR(20) NOT NULL,
ADD COLUMN     "lokasi_kelurahan" VARCHAR(20) NOT NULL,
ADD COLUMN     "lokasi_provinsi" VARCHAR(20) NOT NULL;
