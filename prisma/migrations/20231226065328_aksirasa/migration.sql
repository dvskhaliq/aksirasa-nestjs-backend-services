-- CreateTable
CREATE TABLE "users" (
    "id" VARCHAR(100) NOT NULL,
    "email" VARCHAR(100) NOT NULL,
    "email_verified_at" TIMESTAMPTZ,
    "password" TEXT NOT NULL,
    "created_at" TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
    "status" BOOLEAN DEFAULT false,
    "username" VARCHAR(100) NOT NULL,

    CONSTRAINT "users_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "user_info" (
    "id" VARCHAR(100) NOT NULL,
    "auth_id" VARCHAR(100) NOT NULL,
    "fullname" VARCHAR(100) NOT NULL,
    "phone" CHAR(12) NOT NULL,
    "jenis_kelamin" VARCHAR(5) NOT NULL,

    CONSTRAINT "user_info_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "user_address" (
    "id" VARCHAR(100) NOT NULL,
    "info_id" VARCHAR(100) NOT NULL,
    "address" VARCHAR(300) NOT NULL,
    "command" VARCHAR(100),

    CONSTRAINT "user_address_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "yayasan" (
    "id" VARCHAR(100) NOT NULL,
    "user_id" VARCHAR(100) NOT NULL,
    "fullname_organisasi" VARCHAR(100) NOT NULL,
    "phone" VARCHAR(100) NOT NULL,
    "no_rek" CHAR(12),
    "atas_nama" VARCHAR(100),
    "provinsi" VARCHAR(50),
    "alamat" VARCHAR(100) NOT NULL,
    "tentang_organisasi" TEXT,
    "website" VARCHAR(100),
    "nomer_izin" VARCHAR(100) NOT NULL,
    "link_fb" VARCHAR(100),
    "link_instagram" VARCHAR(100),
    "link_twitter" VARCHAR(100),

    CONSTRAINT "yayasan_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "product_organisasi" (
    "id" VARCHAR(100) NOT NULL,
    "yayasan_id" VARCHAR(100) NOT NULL,
    "judul_product" VARCHAR(100) NOT NULL,
    "harga" DOUBLE PRECISION NOT NULL,
    "description_product" TEXT NOT NULL,

    CONSTRAINT "product_organisasi_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "product_rating" (
    "id" VARCHAR(100) NOT NULL,
    "product_id" TEXT NOT NULL,
    "product_rating" DECIMAL(30,2) NOT NULL,
    "comment" VARCHAR(150) NOT NULL,
    "like" INTEGER NOT NULL,
    "dislike" INTEGER NOT NULL,

    CONSTRAINT "product_rating_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "product_gallery" (
    "id" VARCHAR(100) NOT NULL,
    "product_id" VARCHAR(100) NOT NULL,
    "name_file" VARCHAR(100) NOT NULL,
    "command" VARCHAR(100) NOT NULL,

    CONSTRAINT "product_gallery_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "aktivitas_organisasi" (
    "id" VARCHAR(100) NOT NULL,
    "yayasan_id" VARCHAR(100) NOT NULL,
    "berita_acara" TEXT,
    "judul_berita" VARCHAR(100) NOT NULL,

    CONSTRAINT "aktivitas_organisasi_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "gallery_aktivitas" (
    "id" VARCHAR(100) NOT NULL,
    "aktivitas_id" VARCHAR(100) NOT NULL,
    "name_file" VARCHAR(100) NOT NULL,
    "command" VARCHAR(100) NOT NULL,

    CONSTRAINT "gallery_aktivitas_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "image_list" (
    "id" VARCHAR(100) NOT NULL,
    "aktivitas_id" VARCHAR(100),
    "user_id" VARCHAR(100),
    "yayasan_id" VARCHAR(100),
    "name_file" VARCHAR(100) NOT NULL,
    "command" VARCHAR(100) NOT NULL,

    CONSTRAINT "image_list_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "berkas_list" (
    "id" VARCHAR(100) NOT NULL,
    "yayasan_id" VARCHAR(100) NOT NULL,
    "name_file" VARCHAR(100) NOT NULL,
    "command" VARCHAR(100) NOT NULL,

    CONSTRAINT "berkas_list_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "users_email_key" ON "users"("email");

-- CreateIndex
CREATE UNIQUE INDEX "users_username_key" ON "users"("username");

-- CreateIndex
CREATE UNIQUE INDEX "user_info_auth_id_key" ON "user_info"("auth_id");

-- CreateIndex
CREATE INDEX "user_info_auth_id_idx" ON "user_info"("auth_id");

-- CreateIndex
CREATE INDEX "user_address_info_id_idx" ON "user_address"("info_id");

-- CreateIndex
CREATE UNIQUE INDEX "yayasan_user_id_key" ON "yayasan"("user_id");

-- CreateIndex
CREATE INDEX "yayasan_user_id_idx" ON "yayasan"("user_id");

-- CreateIndex
CREATE INDEX "product_organisasi_yayasan_id_idx" ON "product_organisasi"("yayasan_id");

-- CreateIndex
CREATE INDEX "product_rating_product_id_idx" ON "product_rating"("product_id");

-- CreateIndex
CREATE INDEX "product_gallery_product_id_idx" ON "product_gallery"("product_id");

-- CreateIndex
CREATE INDEX "aktivitas_organisasi_yayasan_id_idx" ON "aktivitas_organisasi"("yayasan_id");

-- CreateIndex
CREATE INDEX "gallery_aktivitas_aktivitas_id_idx" ON "gallery_aktivitas"("aktivitas_id");

-- CreateIndex
CREATE UNIQUE INDEX "image_list_aktivitas_id_key" ON "image_list"("aktivitas_id");

-- CreateIndex
CREATE UNIQUE INDEX "image_list_user_id_key" ON "image_list"("user_id");

-- CreateIndex
CREATE INDEX "image_list_aktivitas_id_idx" ON "image_list"("aktivitas_id");

-- CreateIndex
CREATE INDEX "image_list_user_id_idx" ON "image_list"("user_id");

-- CreateIndex
CREATE INDEX "image_list_yayasan_id_idx" ON "image_list"("yayasan_id");

-- CreateIndex
CREATE INDEX "berkas_list_yayasan_id_idx" ON "berkas_list"("yayasan_id");

-- AddForeignKey
ALTER TABLE "user_info" ADD CONSTRAINT "user_info_auth_id_fkey" FOREIGN KEY ("auth_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "user_address" ADD CONSTRAINT "user_address_info_id_fkey" FOREIGN KEY ("info_id") REFERENCES "user_info"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "yayasan" ADD CONSTRAINT "yayasan_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "product_organisasi" ADD CONSTRAINT "product_organisasi_yayasan_id_fkey" FOREIGN KEY ("yayasan_id") REFERENCES "yayasan"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "product_rating" ADD CONSTRAINT "product_rating_product_id_fkey" FOREIGN KEY ("product_id") REFERENCES "product_organisasi"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "product_gallery" ADD CONSTRAINT "product_gallery_product_id_fkey" FOREIGN KEY ("product_id") REFERENCES "product_organisasi"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "aktivitas_organisasi" ADD CONSTRAINT "aktivitas_organisasi_yayasan_id_fkey" FOREIGN KEY ("yayasan_id") REFERENCES "yayasan"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "gallery_aktivitas" ADD CONSTRAINT "gallery_aktivitas_aktivitas_id_fkey" FOREIGN KEY ("aktivitas_id") REFERENCES "aktivitas_organisasi"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "image_list" ADD CONSTRAINT "image_list_yayasan_id_fkey" FOREIGN KEY ("yayasan_id") REFERENCES "yayasan"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "image_list" ADD CONSTRAINT "image_list_aktivitas_id_fkey" FOREIGN KEY ("aktivitas_id") REFERENCES "aktivitas_organisasi"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "image_list" ADD CONSTRAINT "image_list_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "user_info"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "berkas_list" ADD CONSTRAINT "berkas_list_yayasan_id_fkey" FOREIGN KEY ("yayasan_id") REFERENCES "yayasan"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
