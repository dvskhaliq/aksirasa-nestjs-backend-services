/*
  Warnings:

  - You are about to alter the column `website` on the `yayasan` table. The data in that column could be lost. The data in that column will be cast from `VarChar(100)` to `VarChar(25)`.
  - You are about to alter the column `link_fb` on the `yayasan` table. The data in that column could be lost. The data in that column will be cast from `VarChar(100)` to `VarChar(25)`.
  - You are about to alter the column `link_instagram` on the `yayasan` table. The data in that column could be lost. The data in that column will be cast from `VarChar(100)` to `VarChar(25)`.
  - You are about to alter the column `link_twitter` on the `yayasan` table. The data in that column could be lost. The data in that column will be cast from `VarChar(100)` to `VarChar(25)`.
  - Made the column `phone` on table `yayasan` required. This step will fail if there are existing NULL values in that column.
  - Made the column `no_rek` on table `yayasan` required. This step will fail if there are existing NULL values in that column.
  - Made the column `atas_nama` on table `yayasan` required. This step will fail if there are existing NULL values in that column.
  - Made the column `provinsi` on table `yayasan` required. This step will fail if there are existing NULL values in that column.
  - Made the column `alamat` on table `yayasan` required. This step will fail if there are existing NULL values in that column.
  - Made the column `tentang_organisasi` on table `yayasan` required. This step will fail if there are existing NULL values in that column.
  - Made the column `nomer_izin` on table `yayasan` required. This step will fail if there are existing NULL values in that column.
  - Made the column `tanggal_post` on table `yayasan` required. This step will fail if there are existing NULL values in that column.
  - Made the column `nama_bank` on table `yayasan` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "yayasan" ALTER COLUMN "phone" SET NOT NULL,
ALTER COLUMN "no_rek" SET NOT NULL,
ALTER COLUMN "no_rek" SET DATA TYPE CHAR(16),
ALTER COLUMN "atas_nama" SET NOT NULL,
ALTER COLUMN "provinsi" SET NOT NULL,
ALTER COLUMN "alamat" SET NOT NULL,
ALTER COLUMN "tentang_organisasi" SET NOT NULL,
ALTER COLUMN "website" SET DATA TYPE VARCHAR(25),
ALTER COLUMN "nomer_izin" SET NOT NULL,
ALTER COLUMN "link_fb" SET DATA TYPE VARCHAR(25),
ALTER COLUMN "link_instagram" SET DATA TYPE VARCHAR(25),
ALTER COLUMN "link_twitter" SET DATA TYPE VARCHAR(25),
ALTER COLUMN "tanggal_post" SET NOT NULL,
ALTER COLUMN "nama_bank" SET NOT NULL;
