/*
  Warnings:

  - You are about to drop the column `harga_satuan` on the `transaction_product` table. All the data in the column will be lost.
  - You are about to drop the column `nama_product` on the `transaction_product` table. All the data in the column will be lost.
  - You are about to drop the column `qty` on the `transaction_product` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "transaction_product" DROP COLUMN "harga_satuan",
DROP COLUMN "nama_product",
DROP COLUMN "qty",
ADD COLUMN     "status_pembayaran" BOOLEAN DEFAULT false;

-- CreateTable
CREATE TABLE "order_item" (
    "id" VARCHAR(100) NOT NULL,
    "transaction_id" VARCHAR(100) NOT NULL,
    "nama_product" VARCHAR(100) NOT NULL,
    "qty" INTEGER NOT NULL,
    "harga" MONEY NOT NULL,

    CONSTRAINT "order_item_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "order_item_transaction_id_idx" ON "order_item"("transaction_id");

-- AddForeignKey
ALTER TABLE "order_item" ADD CONSTRAINT "order_item_transaction_id_fkey" FOREIGN KEY ("transaction_id") REFERENCES "transaction_product"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
