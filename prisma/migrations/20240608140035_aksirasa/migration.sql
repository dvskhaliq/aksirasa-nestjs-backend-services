/*
  Warnings:

  - Made the column `status_confirmed` on table `yayasan` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "yayasan" ALTER COLUMN "status_confirmed" SET NOT NULL;
