/*
  Warnings:

  - You are about to alter the column `tanggal_post` on the `aktivitas_organisasi` table. The data in that column could be lost. The data in that column will be cast from `BigInt` to `Integer`.
  - You are about to alter the column `tanggal_post` on the `product_organisasi` table. The data in that column could be lost. The data in that column will be cast from `BigInt` to `Integer`.
  - You are about to alter the column `tanggal_post` on the `yayasan` table. The data in that column could be lost. The data in that column will be cast from `BigInt` to `Integer`.

*/
-- AlterTable
ALTER TABLE "aktivitas_organisasi" ALTER COLUMN "tanggal_post" SET DATA TYPE INTEGER;

-- AlterTable
ALTER TABLE "product_organisasi" ALTER COLUMN "tanggal_post" SET DATA TYPE INTEGER;

-- AlterTable
ALTER TABLE "yayasan" ALTER COLUMN "tanggal_post" SET DATA TYPE INTEGER;
