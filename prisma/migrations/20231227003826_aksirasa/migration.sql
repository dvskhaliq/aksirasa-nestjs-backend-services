/*
  Warnings:

  - You are about to alter the column `product_id` on the `product_rating` table. The data in that column could be lost. The data in that column will be cast from `Text` to `VarChar(100)`.
  - You are about to alter the column `product_rating` on the `product_rating` table. The data in that column could be lost. The data in that column will be cast from `Decimal(30,2)` to `Integer`.

*/
-- DropForeignKey
ALTER TABLE "product_rating" DROP CONSTRAINT "product_rating_product_id_fkey";

-- AlterTable
ALTER TABLE "product_rating" ALTER COLUMN "product_id" SET DATA TYPE VARCHAR(100),
ALTER COLUMN "product_rating" SET DATA TYPE INTEGER;

-- CreateTable
CREATE TABLE "rating_image" (
    "id" VARCHAR(100) NOT NULL,
    "rating_id" VARCHAR(100) NOT NULL,
    "name_file" VARCHAR(100) NOT NULL,
    "command" VARCHAR(100),

    CONSTRAINT "rating_image_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "rating_image_rating_id_idx" ON "rating_image"("rating_id");

-- AddForeignKey
ALTER TABLE "product_rating" ADD CONSTRAINT "product_rating_product_id_fkey" FOREIGN KEY ("product_id") REFERENCES "product_organisasi"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "rating_image" ADD CONSTRAINT "rating_image_rating_id_fkey" FOREIGN KEY ("rating_id") REFERENCES "product_rating"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
