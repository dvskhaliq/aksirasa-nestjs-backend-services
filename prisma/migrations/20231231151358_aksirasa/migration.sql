-- CreateTable
CREATE TABLE "transaction_product" (
    "id" VARCHAR(100) NOT NULL,
    "product_id" VARCHAR(100) NOT NULL,
    "yayasan_id" VARCHAR(100) NOT NULL,
    "user_id" VARCHAR(100) NOT NULL,
    "nama_product" VARCHAR(100) NOT NULL,
    "qty" INTEGER NOT NULL,
    "harga_satuan" MONEY NOT NULL,
    "alamat_pengirim" TEXT NOT NULL,
    "notes" VARCHAR(100) NOT NULL,

    CONSTRAINT "transaction_product_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "cart" (
    "id" VARCHAR(100) NOT NULL,
    "yayasan_id" VARCHAR(100) NOT NULL,
    "user_id" VARCHAR(100) NOT NULL,
    "nama_product" VARCHAR(100) NOT NULL,
    "harga" MONEY NOT NULL,
    "qty" INTEGER NOT NULL,

    CONSTRAINT "cart_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "transaction_product_product_id_idx" ON "transaction_product"("product_id");

-- CreateIndex
CREATE INDEX "transaction_product_user_id_idx" ON "transaction_product"("user_id");

-- CreateIndex
CREATE INDEX "transaction_product_yayasan_id_idx" ON "transaction_product"("yayasan_id");

-- CreateIndex
CREATE INDEX "cart_user_id_idx" ON "cart"("user_id");

-- CreateIndex
CREATE INDEX "cart_yayasan_id_idx" ON "cart"("yayasan_id");

-- AddForeignKey
ALTER TABLE "transaction_product" ADD CONSTRAINT "transaction_product_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "transaction_product" ADD CONSTRAINT "transaction_product_yayasan_id_fkey" FOREIGN KEY ("yayasan_id") REFERENCES "yayasan"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "transaction_product" ADD CONSTRAINT "transaction_product_product_id_fkey" FOREIGN KEY ("product_id") REFERENCES "product_organisasi"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "cart" ADD CONSTRAINT "cart_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "cart" ADD CONSTRAINT "cart_yayasan_id_fkey" FOREIGN KEY ("yayasan_id") REFERENCES "yayasan"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
