/*
  Warnings:

  - You are about to drop the column `privinsi_name` on the `provinsi` table. All the data in the column will be lost.
  - Added the required column `provinsi_name` to the `provinsi` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "provinsi" DROP COLUMN "privinsi_name",
ADD COLUMN     "provinsi_name" VARCHAR(100) NOT NULL;
