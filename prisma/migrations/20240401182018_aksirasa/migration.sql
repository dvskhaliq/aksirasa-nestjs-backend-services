-- AlterTable
ALTER TABLE "aktivitas_organisasi" ALTER COLUMN "lokasi_alamat" DROP NOT NULL,
ALTER COLUMN "lokasi_kecamatan" DROP NOT NULL,
ALTER COLUMN "lokasi_kelurahan" DROP NOT NULL,
ALTER COLUMN "lokasi_provinsi" DROP NOT NULL;
