/*
  Warnings:

  - Added the required column `kode_transaksi` to the `transaction_product` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "transaction_product" ADD COLUMN     "kode_transaksi" VARCHAR(100) NOT NULL;
