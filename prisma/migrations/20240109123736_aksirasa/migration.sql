/*
  Warnings:

  - You are about to drop the column `yayasan_id` on the `transaction_product` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "transaction_product" DROP CONSTRAINT "transaction_product_yayasan_id_fkey";

-- DropIndex
DROP INDEX "transaction_product_yayasan_id_idx";

-- AlterTable
ALTER TABLE "order_item" ADD COLUMN     "yayasan_id" VARCHAR(100),
ALTER COLUMN "product_id" DROP NOT NULL;

-- AlterTable
ALTER TABLE "transaction_product" DROP COLUMN "yayasan_id";

-- CreateIndex
CREATE INDEX "order_item_yayasan_id_idx" ON "order_item"("yayasan_id");

-- AddForeignKey
ALTER TABLE "order_item" ADD CONSTRAINT "order_item_yayasan_id_fkey" FOREIGN KEY ("yayasan_id") REFERENCES "yayasan"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
