/*
  Warnings:

  - The `tanggal_post` column on the `aktivitas_organisasi` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - The `tanggal_post` column on the `product_organisasi` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - The `tanggal_post` column on the `yayasan` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "aktivitas_organisasi" DROP COLUMN "tanggal_post",
ADD COLUMN     "tanggal_post" BIGINT;

-- AlterTable
ALTER TABLE "product_organisasi" DROP COLUMN "tanggal_post",
ADD COLUMN     "tanggal_post" BIGINT;

-- AlterTable
ALTER TABLE "yayasan" DROP COLUMN "tanggal_post",
ADD COLUMN     "tanggal_post" BIGINT;
