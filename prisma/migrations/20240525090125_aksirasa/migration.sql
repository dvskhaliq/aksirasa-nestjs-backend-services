/*
  Warnings:

  - You are about to drop the `user_address` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `address` to the `user_info` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "user_address" DROP CONSTRAINT "user_address_info_id_fkey";

-- AlterTable
ALTER TABLE "user_info" ADD COLUMN     "address" VARCHAR(300) NOT NULL;

-- AlterTable
ALTER TABLE "users" ADD COLUMN     "role" VARCHAR(50);

-- DropTable
DROP TABLE "user_address";
