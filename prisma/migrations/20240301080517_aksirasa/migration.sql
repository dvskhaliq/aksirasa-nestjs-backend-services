/*
  Warnings:

  - You are about to drop the `categoryProduct` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "categoryProduct";

-- CreateTable
CREATE TABLE "category_product" (
    "id" SERIAL NOT NULL,
    "category_name" VARCHAR(100) NOT NULL,

    CONSTRAINT "category_product_pkey" PRIMARY KEY ("id")
);
