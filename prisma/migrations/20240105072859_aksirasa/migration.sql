/*
  Warnings:

  - A unique constraint covering the columns `[kode_transaksi]` on the table `transaction_product` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "transaction_product_kode_transaksi_key" ON "transaction_product"("kode_transaksi");
